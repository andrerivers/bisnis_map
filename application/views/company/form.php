<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Pilih Form Bisnis</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="form-control" id="form" name="form" onchange="location = this.value;">
                                    <option selected disabled value="">Pilih Form</option>
                                    <option value="<?= base_url('bisnis_pemerintah/create'); ?>">Form Bisnis Pemerintah
                                        (Khusus Kantor Pengelola RKUD)</option>
                                    <option value="<?= base_url('bisnis_pendidikan/create'); ?>">Form Bisnis Pendidikan
                                    </option>
                                    <option value="<?= base_url('bisnis_kesehatan/create'); ?>">Form Bisnis Kesehatan
                                        (Rumah Sakit / BLUD)
                                    </option>
                                    <option value="<?= base_url('bisnis_pasar/create'); ?>">Form Bisnis Pasar</option>
                                    <option value="<?= base_url('bisnis_bumn/create'); ?>">Form Bisns BUMN/BUMD</option>
                                    <option value="<?= base_url('bisnis_perkebunan/create'); ?>">Form Bisnis
                                        Perkebunan/Pertanian/Peternakan/Perikanan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="space-8"></div>

                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
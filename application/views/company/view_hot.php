<div class="row">
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue"><?= $title; ?></h3>

        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
        <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">
            Data Instansi
        </div>

        <!-- div.table-responsive -->

        <!-- div.dataTables_borderWrap -->
        <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Instansi</th>
                        <th>Category Business</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>kabupaten/Kota</th>
                        <th>Kecamatan</th>
                        <th>Pipeline</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($bm as $nsb) : ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $nsb['nama_instansi']; ?></td>
                            <td>
                                <?php
                                if ($nsb['id_kategori_bisnis'] == 20) { ?>
                                    <span class="label label-success arrowed"><?= $nsb['kategori_bisnis']; ?> </span>

                                <?php } elseif ($nsb['id_kategori_bisnis'] == 21) { ?>
                                    <span class="label label-info arrowed"><?= $nsb['kategori_bisnis']; ?></span>

                                <?php } elseif ($nsb['id_kategori_bisnis'] == 22) { ?>
                                    <span class="label label-warning arrowed"><?= $nsb['kategori_bisnis']; ?></span>


                                <?php } elseif ($nsb['id_kategori_bisnis'] == 23) { ?>
                                    <span class="label label-danger arrowed"><?= $nsb['kategori_bisnis']; ?></span>

                                <?php } elseif ($nsb['id_kategori_bisnis'] == 24) { ?>
                                    <span class="label label-inverse arrowed"><?= $nsb['kategori_bisnis']; ?></span>

                                <?php  } elseif ($nsb['id_kategori_bisnis'] == 25) { ?>
                                    <span class="label label-purple arrowed"><?= $nsb['kategori_bisnis']; ?></span>
                                <?php } ?>
                            </td>

                            <td><?= $nsb['no_hp_instansi']; ?></td>
                            <td><?= $nsb['email_instansi']; ?></td>
                            <td><?= $nsb['kota']; ?> </td>
                            <td><?= $nsb['kecamatan']; ?></td>
                            <td>

                                <?php
                                if ($nsb['sts_pipeline'] == 0) { ?>
                                    <span class="label label-success arrowed">Draft</span>

                                <?php } elseif ($nsb['sts_pipeline'] == 1) { ?>
                                    <span class="label label-info arrowed">Hot</span>

                                <?php } elseif ($nsb['sts_pipeline'] == 2) { ?>
                                    <span class="label label-warning arrowed">cold</span>
                                <?php } ?>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-cog"></i> <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="<?= base_url('sales/buat_maps/'); ?><?= $nsb['uid_form'];  ?>">
                                                Buat Maps
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?= base_url('company/viewProgress/'); ?><?= $nsb['uid_form'];  ?>">
                                                Progress
                                            </a>
                                        </li>


                                        <?php if ($this->session->userdata('role_id') == 2) { ?>
                                            <li>
                                                <a href="<?= base_url('company/createProgress/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Buat Progress
                                                </a>
                                            </li>
                                        <?php } ?>

                                        <?php
                                        if ($nsb['id_kategori_bisnis'] == 20) { ?>
                                            <li>
                                                <a href="<?= base_url('bisnis_pemerintah/detail/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Detail
                                                </a>
                                            </li>
                                            <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                <li>
                                                    <a href="<?= base_url('bisnis_pemerintah/update/'); ?><?= $nsb['uid_form'];  ?>">
                                                        Ubah
                                                    </a>
                                                </li>
                                        <?php }
                                        } ?>

                                        <?php
                                        if ($nsb['id_kategori_bisnis'] == 21) { ?>
                                            <li>
                                                <a href="<?= base_url('bisnis_pendidikan/detail/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Detail
                                                </a>
                                            </li>
                                            <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                <li>
                                                    <a href="<?= base_url('bisnis_pendidikan/update/'); ?><?= $nsb['uid_form'];  ?>">
                                                        Ubah
                                                    </a>
                                                </li>
                                        <?php }
                                        } ?>


                                        <?php
                                        if ($nsb['id_kategori_bisnis'] == 22) { ?>
                                            <li>
                                                <a class="blue" href="<?= base_url('bisnis_kesehatan/detail/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Detail
                                                </a>
                                            </li>
                                            <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                <li>
                                                    <a href="<?= base_url('bisnis_kesehatan/update/'); ?><?= $nsb['uid_form'];  ?>" title="update">
                                                        Ubah
                                                    </a>
                                                </li>
                                        <?php }
                                        } ?>

                                        <?php
                                        if ($nsb['id_kategori_bisnis'] == 23) { ?>
                                            <li>
                                                <a href="<?= base_url('bisnis_pasar/detail/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Detail
                                                </a>
                                            </li>
                                            <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                <li>
                                                    <a href="<?= base_url('bisnis_pasar/update/'); ?><?= $nsb['uid_form'];  ?>">
                                                        Ubah
                                                    </a>
                                                </li>
                                        <?php }
                                        } ?>

                                        <?php
                                        if ($nsb['id_kategori_bisnis'] == 24) { ?>
                                            <li>
                                                <a href="<?= base_url('bisnis_bumn/detail/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Detail
                                                </a>
                                            </li>
                                            <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                <li>
                                                    <a href="<?= base_url('bisnis_bumn/update/'); ?><?= $nsb['uid_form'];  ?>">
                                                        Ubah
                                                    </a>
                                                </li>
                                        <?php }
                                        } ?>


                                        <?php
                                        if ($nsb['id_kategori_bisnis'] == 25) { ?>
                                            <li>
                                                <a href="<?= base_url('bisnis_perkebunan/detail/'); ?><?= $nsb['uid_form'];  ?>">
                                                    Detail
                                                </a>
                                            </li>
                                            <?php if ($this->session->userdata('role_id') == 2) { ?>
                                                <li>
                                                    <a href="<?= base_url('bisnis_perkebunan/update/'); ?><?= $nsb['uid_form'];  ?>">
                                                        Ubah
                                                    </a>
                                                </li>
                                        <?php }
                                        } ?>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
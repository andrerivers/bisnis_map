<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <div>
                    <div id="user-profile-1" class="user-profile row">
                        <div class="col-xs-12 col-sm-12 center">
                            <div class="space-10"></div>
                            <div>
                                <span class="profile-picture" style="width:10%">
                                    <img id="avatar" class="editable img-responsive" src="<?= base_url('archive/') . $d['foto']; ?>" />
                                </span>

                                <div class="space-4"></div>

                                <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                    <div class="inline position-relative">
                                        <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                            <i class="ace-icon fa fa-circle light-green"></i>
                                            &nbsp;
                                            <span class="white"><?= $d['nama_instansi']; ?></span>
                                        </a>

                                        <ul class="align-left dropdown-menu dropdown-caret dropdown-lighter">
                                            <li class="dropdown-header"> Change Status </li>

                                            <li>
                                                <a href="#">
                                                    <i class="ace-icon fa fa-circle green"></i>
                                                    &nbsp;
                                                    <span class="green">Available</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#">
                                                    <i class="ace-icon fa fa-circle red"></i>
                                                    &nbsp;
                                                    <span class="red">Busy</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#">
                                                    <i class="ace-icon fa fa-circle grey"></i>
                                                    &nbsp;
                                                    <span class="grey">Invisible</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="space-10"></div>
                                <a href="javascript:history.back()" class="btn btn-white btn-info btn-bold">
                                    <i class="ace-icon fa fa-backward bigger-120 blue"></i>
                                    Back
                                </a>

                                <a href="<?= base_url('company/viewProgress/'); ?><?= $d['uid_form'];  ?>" class="btn btn-white btn-danger btn-bold">
                                    <i class="ace-icon fa fa-search bigger-120 blue"></i>
                                    Progress Sales
                                </a>

                            </div>

                            <div class="space-6"></div>
                        </div>

                        <div class="col-xs-12 col-sm-12">
                            <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                    <div class="profile-info-name"><b> I </b> </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><b> IDENTITAS INSTANSI </b></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Nama Instansi </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><?= $d['nama_instansi']; ?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Bidang Usaha </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['kategori_bisnis']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Alamat </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><?= $d['alamat_instansi']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Provinsi </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="country" style="text-transform: uppercase;"><?= $d['provinsi']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kota </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="age" style="text-transform: uppercase;"><?= $d['kota']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kecamatan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="signup"><?= $d['kecamatan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kelurahan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="login" style="text-transform: uppercase;"><?= $d['kelurahan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kode Pos </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['kode_pos']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Maps </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><a href="<?= $d['link_maps']; ?>" target="_blank">Lihat Google Maps</a> </span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Phone </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['no_hp_instansi']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Email </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['email_instansi']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Jumlah Pengusaha </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['jumlah_pengusaha']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Jumlah Transaksi </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= number_format($d['jumlah_transaksi']); ?></span>
                                    </div>
                                </div>

                            </div>
                            <div class="space-8"></div>
                        </div>

                        <div class="col-xs-12 col-sm-12">
                            <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                    <div class="profile-info-name"><b> II </b> </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><b> BANK PENGELOLA </b></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Bank Sumut</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['pgl_bank_sumut']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">Bank Lain</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['pgl_bank_lain']; ?></span>
                                    </div>
                                </div>

                            </div>
                            <div class="space-8"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                    <div class="profile-info-name"><b> III </b> </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><b> Nasabah Bank Sumut </b></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">DPK NoA</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['dpk_noa']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">DPK Nominal</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['dpk_nominal']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">Kredit - NoA</div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['kredit_noa']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">Kredit Nominal</div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['kredit_nominal']; ?></span>
                                    </div>
                                </div>



                            </div>
                            <div class="space-8"></div>
                        </div>


                    </div>





                </div>
            </div>
        </div>
    </div>
    <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
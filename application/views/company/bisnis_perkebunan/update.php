<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <div class="widget-main">
                    <form action="<?= base_url('bisnis_perkebunan/updateGo'); ?>" method="post" enctype="multipart/form-data">
                        <div>
                            <h4 class="lighter orange">I. Identitas Instansi</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div>
                            <label for="form-field-8">ID FORM</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="uid_form" name="uid_form" value="<?= $d['uid_form']; ?>" required readonly </div>
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Nama Instansi</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="text" class="form-control" id="nama_instansi" name="nama_instansi" value="<?= $d['nama_instansi']; ?>" required>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Sektor Usaha</label>
                                <select class="form-control" id="sektor_usaha" name="sektor_usaha" required>
                                    <option value="<?= $d['sektor_usaha']; ?>"><?= $d['sektor_usaha']; ?></option>
                                    <option value="Perkebunan">Perkebunan</option>
                                    <option value="Peternakan">Peternakan</option>
                                    <option value="Perikanan">Perikanan</option>
                                    <option value="Perkebunan">Perkebunan</option>
                                </select>
                            </div>


                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Alamat Instansi</label>
                                <small class="text-danger pl-3">*</small>
                                <textarea type="text" class="form-control" id="alamat_instansi" name="alamat_instansi" value="<?= $d['alamat_instansi']; ?>" required><?= $d['alamat_instansi']; ?></textarea>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Provinsi</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="select2-data-array browser-default" id="select2-provinsi" name="id_provinsi" required></select>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Kabupaten/Kota</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="select2-data-array browser-default" id="select2-kabupaten" name="id_kota" required></select>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Kecamatan</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="select2-data-array browser-default" id="select2-kecamatan" name="id_kecamatan" required></select>
                            </div>
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Kelurahan</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="select2-data-array browser-default" id="select2-kelurahan" name="id_kelurahan" required></select>
                            </div>
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Kode Pos</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="<?= $d['kode_pos']; ?>" required>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">No. Phone Instansi</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="text" class="form-control" id="no_hp_instansi" name="no_hp_instansi" value="<?= $d['no_hp_instansi']; ?>" required>
                            </div>
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Link Maps</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="text" class="form-control" id="link_maps" name="link_maps" value="<?= $d['link_maps']; ?>">
                            </div>
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Email Instansi</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="email" class="form-control" id="email_instansi" name="email_instansi" value="<?= $d['email_instansi']; ?>" required>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Jumlah Pengusaha</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="number" class="form-control" id="jumlah_pengusaha" name="jumlah_pengusaha" value="<?= $d['jumlah_pengusaha']; ?>">
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Jumlah Transaksi</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="number" class="form-control" id="jumlah_transaksi" name="jumlah_transaksi" value="<?= $d['jumlah_transaksi']; ?>">
                            </div>
                            <div class="space-8"></div>

                            <div>
                                <h4 class="lighter orange">II. Nasabah Bank Sumut</h4>
                                <div class="hr hr8"></div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <div>
                                        <label for="form-field-8">Bank Sumut</label>
                                        <select class="form-control" id="pgl_bank_sumut" name="pgl_bank_sumut" required>
                                            <option value="<?= $d['pgl_bank_sumut']; ?>"><?= $d['pgl_bank_sumut']; ?></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div>
                                        <label for="form-field-8">Bank Lain</label>
                                        <select class="form-control" id="pgl_bank_lain" name="pgl_bank_lain" required>
                                            <option value="<?= $d['pgl_bank_lain']; ?>"><?= $d['pgl_bank_lain']; ?></option>
                                            <option value="BNI">BNI</option>
                                            <option value="BRI">BRI</option>
                                            <option value="MANDIRI">MANDIRI</option>
                                            <option value="BSI">BSI</option>
                                            <option value="BTN">BTN</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="space-4"></div>
                                    <div>
                                        <label for="form-field-8">DPK NoA</label>
                                        <select class="form-control" id="dpk_noa" name="dpk_noa" required>
                                            <option value="<?= $d['dpk_noa']; ?>"><?= $d['dpk_noa']; ?></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="space-4"></div>
                                    <div>
                                        <label for="form-field-8">DPK Nominal</label>
                                        <select class="form-control" id="dpk_nominal" name="dpk_nominal" required>
                                            <option value="<?= $d['dpk_nominal']; ?>"><?= $d['dpk_nominal']; ?></option>
                                            <option value="0">0</option>
                                            <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                            <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                            <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                            <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                            <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="space-4"></div>
                                    <div>
                                        <label for="form-field-8">Kredit NoA</label>
                                        <select class="form-control" id="kredit_noa" name="kredit_noa" required>
                                            <option value="<?= $d['kredit_noa']; ?>"><?= $d['kredit_noa']; ?></option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="space-4"></div>
                                    <div>
                                        <label for="form-field-8">Kredit Nominal</label>
                                        <select class="form-control" id="kredit_nominal" name="kredit_nominal" required>
                                            <option value="<?= $d['kredit_nominal']; ?>"><?= $d['kredit_nominal']; ?></option>
                                            <option value="0">0</option>
                                            <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                            <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                            <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                            <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                            <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                        </select>
                                    </div>
                                </div>
                            </div>





                            <div class="space-8"></div>
                            <div>
                                <h4 class="lighter orange">IV. Lampiran</h4>
                                <div class="hr hr8"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div>
                                        <label for="form-field-8">Status Pipeline</label>
                                        <select class="form-control" id="sts_pipeline" name="sts_pipeline" required>
                                            <option selected disabled value="">Pilih</option>
                                            <option value="0">Draft</option>
                                            <option value="1">Hot</option>
                                            <option value="2">Cold</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div>
                                        <label for="form-field-8">Foto</label>
                                        <input type="file" class="form-control" id="foto" name="foto" accept="image/*">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="space-4"></div>
                                    <div>
                                        <label for="form-field-8">File Lampiran</label>
                                        <input type="file" class="form-control" id="file1" name="file1" accept="application/pdf">
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="space-4"></div>
                                    <div>
                                        <label for="form-field-8">Catatan</label>
                                        <textarea type="text" class="form-control" id="catatan" name="catatan" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="space-8"></div>
                            <button type="submit" class="btn btn-white btn-info btn-bold">
                                <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                                Submit
                            </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
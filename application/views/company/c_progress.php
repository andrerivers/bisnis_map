<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <div class="widget-main">
                    <div class="row">
                        <form action="<?= base_url('company/updateProgressGo'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">ID Costumer</label>
                                    <input type="text" class="form-control" id="uid_form" name="uid_form" value="<?= $d['uid_form'] ?>" readonly required>
                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Costumer</label>
                                    <input type="text" class="form-control" id="nama_instansi" name="nama_instansi" value="<?= $d['nama_instansi'] ?>" readonly required>
                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Status Pipeline</label>
                                    <select class="form-control" id="sts_pipeline" name="sts_pipeline" required>
                                        <option selected disabled value="">Pilih</option>
                                        <option value="0">Draft</option>
                                        <option value="1">Hot</option>
                                        <option value="2">Cold</option>
                                    </select>
                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Note</label>
                                    <textarea type="text" class="form-control" id="keterangan" name="keterangan" rows="5" required><?= $d['keterangan'] ?></textarea>
                                </div>


                            </div>

                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Update
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="timeline-container">
                                <div class="timeline-label">
                                    <span class="label label-primary arrowed-in-right label-lg">
                                        <b>History</b>
                                    </span>
                                </div>
                                <?php foreach ($progress as $nsb) : ?>
                                <div class="timeline-items">
                                    <div class="timeline-item clearfix">
                                        <div class="timeline-info">
                                            <i
                                                class="timeline-indicator ace-icon fa fa-user btn btn-success no-hover"></i>
                                        </div>

                                        <div class="widget-box transparent">
                                            <div class="widget-body">
                                                <div class="widget-main">
                                                    Instansi : <?= $nsb['nama_instansi']; ?>  <?php
                                                                    if ($nsb['sts_pipeline'] == 0) {
                                                                        echo '<span class="label label-success arrowed">Draft</span> ';
                                                                    } elseif ($nsb['sts_pipeline'] == 1) {
                                                                        echo '<span class="label label-info arrowed">Hot</span>';
                                                                    } elseif ($nsb['sts_pipeline'] == 3) {
                                                                        echo '<span class="label label-warning arrowed">Cold</span>';
                                                                    }
                                                                    ?>
                                                                    <br>
                                                    <b>Phone : </b>  <?= $nsb['no_hp_instansi']; ?><br>
                                                    <b>email : </b>  <?= $nsb['email_instansi']; ?><br>
                                                    <b>Note :</b> <br>
                                                    <?= $nsb['keterangan']; ?>
                                                    <div class="pull-right">
                                                        <i class="ace-icon fa fa-clock-o bigger-110"></i>
                                                        <?= $nsb['date_created']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.timeline-items -->
                                <?php endforeach; ?>



                            </div><!-- /.timeline-container -->




                        </div>

                    </div>
                    <div class="space-8"></div>
                    <a href="javascript:history.back()" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-backward bigger-120 blue"></i>
                        Kembali
                    </a>

            

                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
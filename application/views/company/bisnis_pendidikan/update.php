<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <div class="widget-main">
                    <form action="<?= base_url('bisnis_pendidikan/updateGo'); ?>" method="post" enctype="multipart/form-data">
                        <div>
                            <h4 class="lighter orange">I. Identitas Instansi</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div>
                            <label for="form-field-8">ID FORM</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="uid_form" name="uid_form" value="<?= $d['uid_form']; ?>" readonly required>
                        </div>

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Nama Instansi</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="nama_instansi" name="nama_instansi" value="<?= $d['nama_instansi']; ?>" required>
                        </div>

                       

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Alamat Instansi</label>
                            <small class="text-danger pl-3">*</small>
                            <textarea type="text" class="form-control" id="alamat_instansi" name="alamat_instansi" required><?= $d['alamat_instansi']; ?></textarea>
                        </div>

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Provinsi</label>
                            <small class="text-danger pl-3">*</small>
                            <select class="select2-data-array browser-default" id="select2-provinsi" name="id_provinsi" required></select>
                        </div>

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Kabupaten/Kota</label>
                            <small class="text-danger pl-3">*</small>
                            <select class="select2-data-array browser-default" id="select2-kabupaten" name="id_kota" required></select>
                        </div>

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Kecamatan</label>
                            <small class="text-danger pl-3">*</small>
                            <select class="select2-data-array browser-default" id="select2-kecamatan" name="id_kecamatan" required></select>
                        </div>
                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Kelurahan</label>
                            <small class="text-danger pl-3">*</small>
                            <select class="select2-data-array browser-default" id="select2-kelurahan" name="id_kelurahan" required></select>
                        </div>
                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Kode Pos</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="<?= $d['kode_pos']; ?>" required>
                        </div>

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">No. Phone Instansi</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="no_hp_instansi" name="no_hp_instansi" value="<?= $d['no_hp_instansi']; ?>" required>
                        </div>
                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Link Maps</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="link_maps" name="link_maps" value="<?= $d['link_maps']; ?>">
                        </div>
                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Email Instansi</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="email" class="form-control" id="email_instansi" name="email_instansi" value="<?= $d['email_instansi']; ?>" required>
                        </div>

                        <div class="space-4"></div>
                        <div>
                            <label for="form-field-8">Jumlah Unit</label>
                            <small class="text-danger pl-3">*</small>
                            <input type="text" class="form-control" id="jumlah_instansi" name="jumlah_instansi" value="<?= $d['jumlah_instansi']; ?>">
                        </div>
                        <div class="space-8"></div>

                        <div>
                            <h4 class="lighter orange">II. Bank Pengelola</h4>
                            <div class="hr hr8"></div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Bank Sumut</label>
                                    <select class="form-control" id="pgl_bank_sumut" name="pgl_bank_sumut" required>
                                        <option value="<?= $d['pgl_bank_sumut']; ?>"><?= $d['pgl_bank_sumut']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Bank Lain</label>
                                    <select class="form-control" id="pgl_bank_lain" name="pgl_bank_lain" required>
                                        <option value="<?= $d['pgl_bank_lain']; ?>"><?= $d['pgl_bank_lain']; ?></option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="MANDIRI">MANDIRI</option>
                                        <option value="BSI">BSI</option>
                                        <option value="BTN">BTN</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="space-8"></div>

                        <div>
                            <h4 class="lighter orange">III. SDM</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Jumlah Pegawai</label>
                                    <input type="text" class="form-control" id="sdm_jumlah_pegawai" value="<?= $d['sdm_jumlah_pegawai']; ?>" name="sdm_jumlah_pegawai" required>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Total Gaji Kepegawaian</label>
                                    <select class="form-control" id="sdm_gaji_total_pegawai" name="sdm_gaji_total_pegawai" required>
                                        <option value="<?= $d['sdm_gaji_total_pegawai']; ?>"><?= $d['sdm_gaji_total_pegawai']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Payroll NoA</label>
                                    <select class="form-control" id="sdm_payroll_noa" name="sdm_payroll_noa" required>
                                        <option value="<?= $d['sdm_payroll_noa']; ?>"><?= $d['sdm_payroll_noa']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Payroll Nominal</label>
                                    <select class="form-control" id="sdm_payroll_nominal" name="sdm_payroll_nominal" required>
                                        <option value="<?= $d['sdm_payroll_nominal']; ?>"><?= $d['sdm_payroll_nominal']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="space-8"></div>
                        <div>
                            <h4 class="lighter orange">IV. Produk Dana dan Jasa Bank</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div class="row">

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tabungan NoA</label>
                                    <select class="form-control" id="pdjb_tabungan_noa" name="pdjb_tabungan_noa" required>
                                        <option value="<?= $d['pdjb_tabungan_noa']; ?>"><?= $d['pdjb_tabungan_noa']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tabungan Nominal</label>
                                    <select class="form-control" id="pdjb_tabungan_nominal" name="pdjb_tabungan_nominal" required>
                                        <option value="<?= $d['pdjb_tabungan_nominal']; ?>"><?= $d['pdjb_tabungan_nominal']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Deposito NoA</label>
                                    <select class="form-control" id="pdjb_deposito_noa" name="pdjb_deposito_noa" required>
                                        <option value="<?= $d['pdjb_deposito_noa']; ?>"><?= $d['pdjb_deposito_noa']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Deposito Nominal</label>
                                    <select class="form-control" id="pdjb_deposito_nominal" name="pdjb_deposito_nominal" required>
                                        <option value="<?= $d['pdjb_deposito_nominal']; ?>"><?= $d['pdjb_deposito_nominal']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tabungan Simpel</label>
                                    <select class="form-control" id="pdjb_tabungan_simpel" name="pdjb_tabungan_simpel" required>
                                        <option value="<?= $d['pdjb_tabungan_simpel']; ?>"><?= $d['pdjb_tabungan_simpel']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tabungan Simpel Nominal</label>
                                    <select class="form-control" id="pdjb_nominal_simpel" name="pdjb_nominal_simpel" required>
                                        <option value="<?= $d['pdjb_nominal_simpel']; ?>"><?= $d['pdjb_nominal_simpel']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="space-8"></div>
                        <div>
                            <h4 class="lighter orange">V. e-Channel</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">CMS</label>
                                    <select type="text" class="form-control" id="e_cms" name="e_cms" required>
                                        <option value="<?= $d['e_cms']; ?>"><?= $d['e_cms']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">EDC</label>
                                    <select type="text" class="form-control" id="e_edc" name="e_edc" required>
                                        <option value="<?= $d['e_edc']; ?>"><?= $d['e_edc']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Merchant QRIS</label>
                                    <select type="text" class="form-control" id="e_merchent_qris" name="e_merchent_qris" required>
                                        <option value="<?= $d['e_merchent_qris']; ?>"><?= $d['e_merchent_qris']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Sumut Mobile</label>
                                    <select type="text" class="form-control" id="e_sumut_mobile" name="e_sumut_mobile" required>
                                        <option value="<?= $d['e_sumut_mobile']; ?>"><?= $d['e_sumut_mobile']; ?></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="space-8"></div>
                        <div>
                            <h4 class="lighter orange">VI. Potensi</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Jumlah Pegawai</label>
                                    <input type="text" class="form-control" id="potensi_jumlah_pegawai" value="<?= $d['potensi_jumlah_pegawai']; ?>" name="potensi_jumlah_pegawai" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Nominal</label>
                                    <select class="form-control" id="potensi_nominal" name="potensi_nominal" required>
                                        <option value="<?= $d['potensi_nominal']; ?>"><?= $d['potensi_nominal']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Jumlah Siswa</label>
                                    <input type="text" class="form-control" id="potensi_jumlah_siswa" value="<?= $d['potensi_jumlah_siswa']; ?>" name="potensi_jumlah_siswa" required>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Nominal</label>
                                    <select class="form-control" id="potensi_nominal_siswa" name="potensi_nominal_siswa" required>
                                        <option value="<?= $d['potensi_nominal_siswa']; ?>"><?= $d['potensi_nominal_siswa']; ?></option>
                                        <option value="0">0</option>
                                        <option value="1 Juta - 10 Juta">1 Juta - 10 Juta</option>
                                        <option value="10 Juta - 100 Juta">10 Juta - 100 Juta</option>
                                        <option value="100 Juta - 500 Juta">100 Juta - 500 Juta</option>
                                        <option value="500 Juta - 1 M">500 Juta - 1 M</option>
                                        <option value="Lebih Dari 1 M">Lebih Dari 1 M</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="space-8"></div>
                        <div>
                            <h4 class="lighter orange">VII. Lampiran</h4>
                            <div class="hr hr8"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Status Pipeline</label>
                                    <select class="form-control" id="sts_pipeline" name="sts_pipeline" required>
                                        <option selected disabled value="">Pilih</option>
                                        <option value="0">Draft</option>
                                        <option value="1">Hot</option>
                                        <option value="2">Cold</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Foto Instansi</label>
                                    <input type="file" class="form-control" id="foto" name="foto" accept="image/*">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">File Lampiran</label>
                                    <input type="file" class="form-control" id="file1" name="file1" accept="application/pdf">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Catatan</label>
                                    <textarea type="text" class="form-control" id="catatan" name="catatan" rows="5"><?= $d['catatan']; ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="space-8"></div>
                        <button type="submit" class="btn btn-white btn-info btn-bold">
                            <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
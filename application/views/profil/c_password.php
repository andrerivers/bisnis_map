<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <?= $this->session->flashdata('message'); ?>
                <div class="widget-main">
                    <div class="row">
                        <form action="<?= base_url('sales/password'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">

                                <div>
                                    <label for="form-field-8">Password Lama</label>
                                    <input type="password" class="form-control" id="current_password" name="current_password" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Password Baru</label>
                                    <input type="password" class="form-control" id="new_password1" name="new_password1" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Ulangi Password Baru</label>
                                    <input type="password" class="form-control" id="new_password2" name="new_password2" required>
                                </div>


                            </div>

                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Simpan
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= validation_errors(); ?>
                </div>
                <?php endif ?>
                <div class="widget-main">
                    <div class="row">
                        <form action="<?= base_url('qualification/updateGo'); ?>" method="post"
                            enctype="multipart/form-data">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">ID Costumer</label>
                                    <input type="text" class="form-control" id="uid_nasabah" name="uid_nasabah"
                                        value="<?= $d['uid_nasabah'] ?>" readonly required>
                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Costumer</label>
                                    <input type="text" class="form-control" id="nama" name="nama"
                                        value="<?= $d['nama'] ?>" readonly required>
                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Due Date</label>
                                    <input type="text" class="form-control" id="due_date" name="due_date"
                                        value="<?= $d['due_date'] ?>" readonly required>
                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Note</label>
                                    <textarea type="text" class="form-control" id="ket_lain" name="ket_lain" rows="5"
                                        required><?= $d['ket_lain'] ?></textarea>
                                </div>


                            </div>

                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Update
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
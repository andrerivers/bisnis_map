<div class="row">
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue"><?= $title; ?></h3>
        <?php if ($this->session->userdata('role_id') == 2) { ?>
            <a href="<?= base_url('prospecting/create'); ?>" class="btn btn-white btn-info btn-bold">
                <i class="ace-icon fa fa-plus bigger-120 blue"></i>
                New Prospecting
            </a>

            <a href="<?= base_url('prospecting/switch'); ?>" class="btn btn-danger">
                <i class="ace-icon fa fa-cog bigger-120 blue"></i>
                Switch Pipeline
            </a>
        <?php } ?>
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
        <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">
            Data Prospecting
        </div>

        <!-- div.table-responsive -->

        <!-- div.dataTables_borderWrap -->
        <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Company</th>
                        <th>Category Business</th>
                        <th>Phone</th>
                        <th>Due Date</th>
                        <th>Note</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($v as $prs) : ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><a href="<?= base_url('sales/nasabah/detail/'); ?><?= $prs['uid_nasabah'];  ?>"><?= $prs['nama']; ?></a>
                            </td>
                            <td><?= $prs['nama_usaha']; ?></td>
                            <td>
                                <?= $prs['kategori_bisnis']; ?> </td>
                            <td><?= $prs['no_hp']; ?></td>
                            <td><?= $prs['due_date']; ?></td>
                            <td><?= $prs['ket_lain']; ?></td>
                            <td> <span class="label label-success arrowed">Prospecting</span></td>
                            <td>
                                <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="blue" href="<?= base_url('prospecting/detail/'); ?><?= $prs['uid_nasabah'];  ?>" title="detail">
                                        <i class="ace-icon fa fa-search-plus bigger-130"></i>
                                    </a>
                                    <?php if ($this->session->userdata('role_id') == 2) { ?>
                                        <a class="green" href="<?= base_url('prospecting/update/'); ?><?= $prs['uid_nasabah'];  ?>" title="update">
                                            <i class="ace-icon fa fa-pencil bigger-130"></i>
                                        </a>
                                    <?php } ?>
                                </div>

                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
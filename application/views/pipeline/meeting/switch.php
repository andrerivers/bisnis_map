<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= validation_errors(); ?>
                </div>
                <?php endif ?>
                <div class="widget-main">
                    <div class="row">
                        <form action="<?= base_url('meeting/switch'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Customer</label>
                                    <select class="chosen-select form-control" id="form-field-select-3"
                                        name="uid_nasabah" data-placeholder="Choose a customer...">
                                        <option selected disabled value="">Pilih</option>
                                        <?php foreach ($nasabah as $bs) : ?>
                                        <option value="<?= $bs['uid_nasabah']; ?>"><?= $bs['nama']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Due Date</label>
                                    <input type="date" class="form-control" id="due_date" name="due_date" required>

                                </div>
                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Switch Pipeline</label>
                                    <select class="form-control" id="form-field-select-3" name="sts"
                                        data-placeholder="Choose a customer...">
                                        <option selected disabled value="">Pilih</option>
                                        <option value="2">Qualification</option>
                                        <option value="3">Meeting</option>
                                        <option value="4">Proposal</option>
                                        <option value="5">Closing</option>
                                        <option value="6">Retention</option>
                                    </select>
                                </div>
                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Support File 1 </label>
                                    <input type="file" class="form-control" id="file1" name="file1"
                                        accept="application/pdf">

                                </div>

                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Support File 2 </label>
                                    <input type="file" class="form-control" id="2" name="2" accept="application/pdf">

                                </div>
                                <div class="space-8"></div>
                                <div>
                                    <label for="form-field-8">Note</label>
                                    <textarea type="text" class="form-control" id="ket_lain" name="ket_lain" rows="5"
                                        required></textarea>
                                </div>


                            </div>

                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Submit
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<div class="row">
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue"><?= $title; ?></h3>
        <a href="<?= base_url('sales/nasabah/create'); ?>" class="btn btn-white btn-info btn-bold">
            <i class="ace-icon fa fa-plus bigger-120 blue"></i>
            New Customer
        </a>

        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
        <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">
            Customers
        </div>

        <!-- div.table-responsive -->

        <!-- div.dataTables_borderWrap -->
        <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Usia</th>
                        <th>Phone</th>
                        <th>Company</th>
                        <th>Addresses</th>
                        <th>Category Business</th>
                        <th>Zip state</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($nasabah as $nsb) : ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $nsb['nama']; ?></td>
                            <td>31</td>
                            <td><?= $nsb['no_hp']; ?></td>
                            <td><?= $nsb['nama_usaha']; ?></td>
                            <td><?= $nsb['alamat']; ?></td>
                            <td>
                                <?= $nsb['kategori_bisnis']; ?> </td>
                            <td><?= $nsb['kode_pos']; ?></td>
                            <td>
                                <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="blue" href="<?= base_url('sales/nasabah/detail/'); ?><?= $nsb['uid_nasabah'];  ?>" title="detail">
                                        <i class="ace-icon fa fa-search-plus bigger-130"></i>
                                    </a>

                                    <a class="green" href="<?= base_url('sales/nasabah/update/'); ?><?= $nsb['uid_nasabah'];  ?>" title="update">
                                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                    <a class="red" href="<?= base_url('sales/nasabah/maping/'); ?><?= $nsb['uid_nasabah'];  ?>" title="Maps">
                                        <i class="ace-icon fa fa-map-marker bigger-130"></i>
                                    </a>

                                    <!-- <a class="red" href="<?= base_url('sales/nasabah/delete/'); ?><?= $nsb['uid_nasabah'];  ?>" title="Delete">
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a> -->
                                </div>

                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
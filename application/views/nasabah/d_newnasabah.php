<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <div>
                    <div id="user-profile-1" class="user-profile row">
                        <div class="col-xs-12 col-sm-12 center">
                            <div class="space-10"></div>
                            <div>
                                <span class="profile-picture">
                                    <img id="avatar" class="editable img-responsive" alt="Alex's Avatar"
                                        src="<?= base_url('assets/thema/') ?>assets/images/avatars/profile-pic.jpg" />
                                </span>

                                <div class="space-4"></div>

                                <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                    <div class="inline position-relative">
                                        <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                            <i class="ace-icon fa fa-circle light-green"></i>
                                            &nbsp;
                                            <span class="white"><?= $d['nama']; ?></span>
                                        </a>

                                        <ul class="align-left dropdown-menu dropdown-caret dropdown-lighter">
                                            <li class="dropdown-header"> Change Status </li>

                                            <li>
                                                <a href="#">
                                                    <i class="ace-icon fa fa-circle green"></i>
                                                    &nbsp;
                                                    <span class="green">Available</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#">
                                                    <i class="ace-icon fa fa-circle red"></i>
                                                    &nbsp;
                                                    <span class="red">Busy</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#">
                                                    <i class="ace-icon fa fa-circle grey"></i>
                                                    &nbsp;
                                                    <span class="grey">Invisible</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="space-10"></div>
                                <a href="javascript:history.back()" class="btn btn-white btn-info btn-bold">
                                    <i class="ace-icon fa fa-backward bigger-120 blue"></i>
                                    Back
                                </a>

                            </div>

                            <div class="space-6"></div>
                        </div>

                        <div class="col-xs-6 col-sm-6">
                            <div class="space-12"></div>

                            <div class="profile-user-info profile-user-info-striped">
                            <div class="profile-info-row">
                                    <div class="profile-info-name"> Uid. Nasabah </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username"><?= $d['uid_nasabah']; ?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Jenis Identitas </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><?= $d['jenis_identitas']; ?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> No. Identitas </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><?= $d['no_identitas']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Nama Lengkap </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="country" style="text-transform: uppercase;"><?= $d['nama_lengkap']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Tempat Lahir </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="age" style="text-transform: uppercase;"><?= $d['tempat_lahir']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Tanggal Lahir </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="signup"><?= $d['tgl_lahir']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Jenis Kelamin </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="login" style="text-transform: uppercase;"><?= $d['jk']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Alamat Rumah </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['alamat']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Provinsi </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['provinsi']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kabupaten </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['kota']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kecamatan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['kecamatan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kelurahan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['kelurahan']; ?></span>
                                    </div>
                                </div>


                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Zip Code </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['kode_pos']; ?></span>
                                    </div>
                                </div>


                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Phone </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['no_hp']; ?></span>
                                    </div>
                                </div>


                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Email </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['email']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Nama Alias </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username"><?= $d['nama_alias']; ?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Kewarganegaraan </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><?= $d['jenis_identitas']; ?></span>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Status Kependudukan </div>
                                    <div class="profile-info-value">
                                        <span class="editable" id="username" style="text-transform: uppercase;"><?= $d['sts_kependudukan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> NPWP </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="country" style="text-transform: uppercase;"><?= $d['npwp']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Status Perkawinan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="age" style="text-transform: uppercase;"><?= $d['sts_perkawinan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Agama </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="signup"><?= $d['agama']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Pendidikan Terakhir </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="login" style="text-transform: uppercase;"><?= $d['pendidikan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Tempat Tinggal </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['sts_tempat_tinggal']; ?></span>
                                    </div>
                                </div>

                            </div>

                            <div class="space-12"></div>

                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <div class="space-12"></div>

                            <div class="profile-user-info profile-user-info-striped">
                           

                                <div class="profile-info-row">
                                    <div class="profile-info-name">Perusahaan  </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about"><?= $d['nama_usaha']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Bidang Usaha </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['kategori_bisnis']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Jabatan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['jabatan']; ?></span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name">Divisi/Bagian </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['divisi']; ?></span>
                                    </div>
                                </div>


                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Pekerjaan Sekarang </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['pekerjaan_sekarang']; ?></span>
                                    </div>
                                </div>


                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Status Pegawai </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['sts_pekerjaan']; ?></span>
                                    </div>
                                </div>


                                  
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Jenis Nasabah </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['jenis_nasabah']; ?></span>
                                    </div>
                                </div>


                                <div class="profile-info-row">
                                    <div class="profile-info-name">No. Rekening </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['no_rekening']; ?></span>
                                    </div>
                                </div>
                                
                                
                                <div class="profile-info-row">
                                    <div class="profile-info-name">Jenis Tabungan </div>

                                    <div class="profile-info-value">
                                        <span class="editable" id="about" style="text-transform: uppercase;"><?= $d['nama_tab']; ?></span>
                                    </div>
                                </div>



                                


                               



                            </div>
                            <div class="space-12"></div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<?php echo $map['js']; ?>
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <div class="widget-main">
                    <div>
                        <h4>I. MAPING</h5>
                            <div class="hr hr8"></div>
                    </div>
                    <div class="row">
                        <form action="<?= base_url('sales/nasabah/updateMap'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">UID Nasabah</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="uid_nasabah" name="uid_nasabah" value="<?= $d['uid_nasabah']; ?>" required readonly>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Nama Lengkap</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $d['nama_lengkap']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Nasabah Bank Sumut</label>
                                    <select class="form-control" id="jenis_nasabah" name="jenis_nasabah">
                                        <option value="<?= $d['jenis_nasabah']; ?>"><?= $d['jenis_nasabah']; ?></option>
                                        <option value="YES">YES</option>
                                        <option value="NO">NO</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Latitude</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="latitude" name="latitude" value="<?= $d['latitude']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Longitude</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="longitude" name="longitude" value="<?= $d['longitude']; ?>" required>
                                </div>
                                <div class="space-8"></div>
                                <button type="submit" class="btn btn-white btn-info btn-bold">
                                    <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                                    Update
                                </button>
                            </div>
                            <div class="col-xs-6">
                                <label class="control-label">Lokasi (Koordinat)</label>
                                <div class="space-4"></div>
                                <div class="panel panel-success">
                                    <div class="panel-heading">Pilih Lokasi Nasabah</div>
                                    <div class="panel-body">

                                        <?php echo $map['html']; ?>

                                    </div>
                                    <div class="panel-footer"></div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
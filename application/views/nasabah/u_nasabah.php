<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <div class="widget-main">
                    <div>
                        <h4>I. Data Pribadi</h5>
                            <div class="hr hr8"></div>
                    </div>
                    <div class="row">
                        <form action="<?= base_url('sales/nasabah/updateGo'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">UID Nasabah</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="uid_nasabah" name="uid_nasabah" value="<?= $d['uid_nasabah']; ?>" required readonly>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Nama Lengkap</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $d['nama']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tempat Lahir</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?= $d['tempat_lahir']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tanggal Lahir</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= $d['tgl_lahir']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Jenis Identitas Utama</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="jenis_identitas" name="jenis_identitas" value="<?= $d['nama']; ?>" required>
                                        <option value="<?= $d['jenis_identitas']; ?>"><?= $d['jenis_identitas']; ?></option>
                                        <option value="KTP">KTP</option>
                                        <option value="SIM">SIM</option>
                                        <option value="PASSPOR">PASSPOR</option>
                                        <option value="Kartu Pelajar/Mahasiswwa">Kartu Pelajar/Mahasiswwa</option>
                                        <option value="KIMS/KITAS/KITAP">KIMS/KITAS/KITAP</option>
                                    </select>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">No. Identitas</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= $d['no_identitas']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Alamat Saat Ini</label>
                                    <small class="text-danger pl-3">*</small>
                                    <textarea type="text" class="form-control" id="alamat" name="alamat" value="<?= $d['alamat']; ?>" required><?= $d['alamat']; ?></textarea>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Provinsi</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="select2-data-array browser-default" id="select2-provinsi" name="id_provinsi"></select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Kabupaten/Kota</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="select2-data-array browser-default" id="select2-kabupaten" name="id_kota"></select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Kecamatan</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="select2-data-array browser-default" id="select2-kecamatan" name="id_kecamatan"></select>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Kelurahan</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="select2-data-array browser-default" id="select2-kelurahan" name="id_kelurahan"></select>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Kode Pos</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="<?= $d['kode_pos']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">No. Phone</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= $d['no_hp']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Email</label>
                                    <small class="text-danger pl-3">*</small>
                                    <input type="email" class="form-control" id="email" name="email" value="<?= $d['email']; ?>" required>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">Nama Alias</label>
                                    <input type="text" class="form-control" id="form-field-8" id="nama_alias" value="<?= $d['nama_alias']; ?>" name="nama_alias" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Jenis Kelamin</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="jk" name="jk" required>
                                        <option value="<?= $d['jk']; ?>"><?= $d['jk']; ?></option>
                                        <option value="Pria">Pria</option>
                                        <option value="Wanita">Wanita</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Kewarganegaraan</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="kewarganegaraan" name="kewarganegaraan" required>
                                        <option value="<?= $d['kewarganegaraan']; ?>"><?= $d['kewarganegaraan']; ?>
                                        </option>
                                        <option value="Indonesia">Indonesia</option>
                                        <option value="Asia">Asia</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Status Kependudukan</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="sts_kependudukan" name="sts_kependudukan" required>
                                        <option value="<?= $d['sts_kependudukan']; ?>"><?= $d['sts_kependudukan']; ?>
                                        </option>
                                        <option value="Penduduk">Penduduk</option>
                                        <option value="Non Penduduk">Non Penduduk</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">NPWP</label>
                                    <input type="number" class="form-control" id="npwp" name="npwp" value="<?= $d['npwp']; ?>">
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Status Perkawinan</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="sts_perkawinan" name="sts_perkawinan" required>
                                        <option value="<?= $d['sts_perkawinan']; ?>"><?= $d['sts_perkawinan']; ?>
                                        </option>
                                        <option value="Menikah">Menikah</option>
                                        <option value="Lajang">Lajang</option>
                                        <option value="Duda/Janda">Duda/Janda</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Agama</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="agama" name="agama" required>
                                        <option value="<?= $d['agama']; ?>"><?= $d['agama']; ?></option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Protestan">Protestan</option>
                                        <option value="Kong Hu Cu">Kong Hu Cu</option>
                                        <option value="Katholik">Katholik</option>
                                        <option value="Hindu">Hindu</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Pendidikan Terakhir</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="pendidikan" name="pendidikan" required>
                                        <option value="<?= $d['pendidikan']; ?>"><?= $d['pendidikan']; ?></option>
                                        <option value="SD">SD</option>
                                        <option value="SMP">SMP</option>
                                        <option value="SMA/K">SMA/K</option>
                                        <option value="S1">S1</option>
                                        <option value="S2">S2</option>
                                        <option value="S3">S3</option>

                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Status Tempat Tinggal</label>
                                    <small class="text-danger pl-3">*</small>
                                    <select class="form-control" id="sts_tempat_tinggal" name="sts_tempat_tinggal" required>
                                        <option value="<?= $d['sts_tempat_tinggal']; ?>">
                                            <?= $d['sts_tempat_tinggal']; ?></option>
                                        <option value="Milik Sendiri">Milik Sendiri</option>
                                        <option value="Milik Keluarga">Milik Keluarga</option>
                                        <option value="Sewa/Kontrakan">Sewa/Kontrakan</option>
                                        <option value="Dinas/instansi">Dinas/instansi</option>

                                    </select>
                                </div>
                            </div>
                    </div>
                    <div class="space-16"></div>
                    <div>
                        <h4>II. Pekerjaan</h5>
                            <div class="hr hr8"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div>
                                <label for="form-field-8">Nama Perusahaan/Instansi</label>
                                <input type="text" class="form-control" id="form-field-8" id="nama_usaha" name="nama_usaha" value="<?= $d['nama_usaha']; ?>" required>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Bidang Usaha</label>
                                <select class="chosen-select form-control" id="form-field-select-3" name="id_bisnis" data-placeholder="Choose a Business...">
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($bisnis as $bs) : ?>
                                        <option value="<?= $bs['id_bisnis']; ?>"><?= $bs['kategori_bisnis']; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Jabatan</label>
                                <input type="text" class="form-control" id="jabatan" name="jabatan" value="<?= $d['jabatan']; ?>">
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Divisi/Bagian</label>
                                <input type="text" class="form-control" id="divisi" name="divisi" value="<?= $d['divisi']; ?>">
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Tanggal Mulai Bekerja</label>
                                <input type="date" class="form-control" id="tgl_mulai_kerja" name="tgl_mulai_kerja" value="<?= $d['tgl_mulai_bekerja']; ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div>
                                <label for="form-field-8">Sumber Pendapatan</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="form-control" id="sumber_pendapatan" name="sumber_pendapatan">
                                    <option value="<?= $d['sumber_pendapatan']; ?>"><?= $d['sumber_pendapatan']; ?>
                                    </option>
                                    <option value="Gaji">Gaji</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Pendapatan */Bulan</label>
                                <small class="text-danger pl-3">*</small>
                                <input type="text" class="form-control" id="pendapatan" name="pendapatan" value="<?= $d['nama']; ?>">
                            </div>
                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Pekerjaan Sekarang</label>
                                <small class="text-danger pl-3">*</small>
                                <select class="form-control" id="pekerjaan_sekarang" name="pekerjaan_sekarang">
                                    <option value="<?= $d['pekerjaan_sekarang']; ?>"><?= $d['pekerjaan_sekarang']; ?>
                                    </option>
                                    <option value="Wiraswasta">Wiraswasta</option>
                                    <option value="Swasta">Swasta</option>
                                    <option value="PNS/TNI/POLRI">PNS/TNI/POLRI</option>
                                    <option value="Penyelenggara Negara">Penyelenggara Negara</option>
                                    <option value="BUMN/BUMD">BUMN/BUMD</option>
                                    <option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
                                    <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                            <div>
                                <label for="form-field-8">Status Pekerjaan</label>
                                <select class="form-control" id="sts_pekerjaan" name="sts_pekerjaan">
                                    <option value="<?= $d['sts_pekerjaan']; ?>"><?= $d['sts_pekerjaan']; ?></option>
                                    <option value="Tetap">Tetap</option>
                                    <option value="Kontrak">Kontrak</option>
                                    <option value="Paruh Waktu">Paruh Waktu</option>
                                    <option value="Honorer">Honorer</option>
                                    <option value="Lainnya">Lainnya</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="space-16"></div>

                    <div>
                        <h4>III. Tabungan</h5>
                            <div class="hr hr8"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div>
                                <label for="form-field-8">Jenis Nasabah Bank Sumut</label>
                                <select class="form-control" id="jenis_nasabah" name="jenis_nasabah">
                                    <option value="<?= $d['jenis_nasabah']; ?>"><?= $d['jenis_nasabah']; ?></option>
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>

                                </select>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">No Rekening</label>
                                <input type="text" class="form-control" id="no_rekening" name="no_rekening" value="<?= $d['no_rekening']; ?>">
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Jenis Tabungan</label>
                                <select class="form-control" id="id_tabungan" name="id_tabungan">
                                    <option selected disabled value="">Pilih</option>
                                    <?php foreach ($jt as $jj) : ?>
                                        <option value="<?= $jj['id_tab']; ?>"><?= $jj['nama_tab']; ?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div>
                                <label for="form-field-8">Nasabah Bank Lain</label>
                                <select class="form-control" id="jenis_nasabah_lain" name="jenis_nasabah_lain">
                                    <option value="<?= $d['jenis_nasabah_lain']; ?>"><?= $d['jenis_nasabah_lain']; ?></option>
                                    <option value="BNI">BNI</option>
                                    <option value="BRI">BRI</option>
                                    <option value="MANDIRI">MANDIRI</option>
                                    <option value="BSI">BSI</option>
                                    <option value="BTN">BTN</option>

                                </select>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">No Rekening Lain</label>
                                <input type="text" class="form-control" id="no_rekening_lain" name="no_rekening_lain" value="<?= $d['no_rekening_lain']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Submit
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div class="row">

                        <div class="col-xs-6">
                            <div>
                                <label for="form-field-8">no_identitas</label>
                                <input type="text" class="form-control" id="no_identitas" name="no_identitas" value="<?= $d['no_identitas']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Name</label>
                                <input type="text" class="form-control" id="nama" name="nama" value="<?= $d['nama']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Birthplace</label>
                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?= $d['tempat_lahir']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Birth Date</label>
                                <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= $d['tgl_lahir']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Gender</label>
                                <input type="date" class="form-control" id="jk" name="jk" value="<?= $d['jk']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">home address</label>
                                <input type="date" class="form-control" id="jk" name="jk" value="<?= $d['alamat']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">City</label>
                                <input type="date" class="form-control" id="jk" name="jk" value="<?= $d['kota']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Kelurahan</label>
                                <input type="date" class="form-control" id="jk" name="jk" value="<?= $d['kelurahan']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Kecamatan</label>
                                <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="<?= $d['kecamatan']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Zip Kode</label>
                                <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="<?= $d['kode_pos']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Phone</label>
                                <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= $d['no_hp']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="<?= $d['email']; ?>" readonly>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div>
                                <label for="form-field-8">Company</label>
                                <input type="text" class="form-control" id="form-field-8" id="nama_usaha" name="nama_usaha" value="<?= $d['nama_usaha']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Category Business</label>
                                <input type="text" class="form-control" id="form-field-8" id="kategori_bisnis" name="kategori_bisnis" value="<?= $d['kategori_bisnis']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Phone company</label>
                                <input type="number" class="form-control" id="no_hp_usaha" name="no_hp_usaha" value="<?= $d['no_hp_usaha']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Email company</label>
                                <input type="email" class="form-control" id="email_usaha" name="email_usaha" value="<?= $d['email_usaha']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Url Location</label>
                                <input type="text" class="form-control" id="url_usaha" name="url_usaha" value="<?= $d['url_usaha']; ?>" readonly>
                            </div>

                            <div class="space-4"></div>
                            <div>
                                <label for="form-field-8">Addreses company</label>
                                <textarea type="text" class="form-control" id="alamat_usaha" name="alamat_usaha" value="<?= $d['alamat_usaha']; ?>" readonly><?= $d['alamat_usaha']; ?></textarea>
                            </div>

                        </div>

                    </div>
                    <div class="space-8"></div>
                    <a href="javascript:history.back()" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-backward bigger-120 blue"></i>
                        Back
                    </a>

                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
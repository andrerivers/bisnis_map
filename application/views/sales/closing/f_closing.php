<!-- Main content -->
<!-- Main content -->
<div class="content">
    <div class="container">

        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                            title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    <form class="form-horizontal">
                        <div class="card-body">
                            <h3 class="card-title">Form Contact</h3>
                            <div class="table-responsive">
                                <div class="form-group">
                                    <label for="pwd">Pilih Form:</label>
                                    <select class="form-control show-tick" name="form"
                                        onchange="location = this.value;">
                                        <option selected disabled value="">Pilih Form</option>
                                        <option value="<?= base_url('sales/f_contact'); ?>">Form Contact</option>
                                        <option value="<?= base_url('sales/f_qualification'); ?>">Form Qualification
                                        </option>
                                        <option value="<?= base_url('sales/f_meeting'); ?>">Form Meeting</option>
                                        <option value="<?= base_url('sales/f_proposal'); ?>">Form Proposal</option>
                                        <option value="<?= base_url('sales/f_closing'); ?>">Form Closing</option>
                                        <option value="<?= base_url('sales/f_retention'); ?>">Form Retention</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</div>
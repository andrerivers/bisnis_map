<div class="row">
    <div class="col-xs-12">
        <h3 class="header smaller lighter orange"><?= $title; ?></h3>
        <a href="<?= base_url('staf/create'); ?>" class="btn btn-white btn-info btn-bold">
            <i class="ace-icon fa fa-plus bigger-120 blue"></i>
            Tambah Data
        </a>

        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
        <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">
            Data User
        </div>

        <!-- div.table-responsive -->

        <!-- div.dataTables_borderWrap -->
        <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>No. HP</th>
                        <th>Unit Kerja</th>
                        <th>Keaktifan</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($v as $nsb) : ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $nsb['nip']; ?></td>
                            <td><?= $nsb['nama']; ?></td>
                            <td><?= $nsb['no_hp']; ?></td>
                            <td><?= $nsb['nama_kcp']; ?></td>
                            <td>
                                <?php
                                if ($nsb['is_active'] == 1) { ?>
                                    <span class="label label-success arrowed">Aktif </span>
                                <?php } else { ?>
                                    <span class="label label-danger arrowed">Nonaktif</span>
                                <?php }  ?>
                            </td>
                            <td>
                                <?php
                                if ($nsb['role_id'] == 1) { ?>
                                    <span class="label label-success arrowed">Admin Pusta </span>
                                <?php } elseif ($nsb['role_id'] == 2) { ?>
                                    <span class="label label-danger arrowed">User</span>
                                <?php } else {  ?>
                                    <span class="label label-warning arrowed">Admin Cabang</span>
                                <?php }  ?>
                            </td>
                            <td>
                                <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="black" href="<?= base_url('staf/update/'); ?><?= $nsb['id_user'];  ?>" title="Update">
                                        <i class="ace-icon fa fa-edit bigger-130"></i>
                                    </a>
                                    <a class="blue" href="<?= base_url('staf/detail/'); ?><?= $nsb['id_user'];  ?>" title="Detail">
                                        <i class="ace-icon fa fa-eye bigger-130"></i>
                                    </a>

                                    <?php
                                    if ($nsb['is_active'] == 1) { ?>
                                        <a class="red" href="<?= base_url('staf/nonaktif/'); ?><?= $nsb['id_user'];  ?>" title="Non Aktif">
                                            <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a class="green" href="<?= base_url('staf/aktif/'); ?><?= $nsb['id_user'];  ?>" title="Aktif">
                                            <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                    <?php }  ?>

                                </div>

                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<div class="row">
    <div class="col-xs-12">
        <h3 class="header smaller lighter orange"><?= $title; ?></h3>
        <a href="<?= base_url('kcp/create'); ?>" class="btn btn-white btn-info btn-bold">
            <i class="ace-icon fa fa-plus bigger-120 blue"></i>
            Tambah Data
        </a>

        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('sukses'); ?>"></div>
        <div class="flash-data-gagal" data-flashdatagagal="<?= $this->session->flashdata('gagal'); ?>"></div>

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
        <div class="table-header">
            Data KCP
        </div>

        <!-- div.table-responsive -->

        <!-- div.dataTables_borderWrap -->
        <div>
            <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kode KCP</th>
                        <th>Nama Cabang</th>
                        <th>Nama KCP</th>
                        <th>Keaktifan</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($kc as $nsb) : ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $nsb['kode_kcp']; ?></td>
                            <td><?= $nsb['nama_cabang']; ?></td>
                            <td><?= $nsb['nama_kcp']; ?></td>
                            <td>
                                <?php
                                if ($nsb['aktif'] == 1) { ?>
                                    <span class="label label-success arrowed">Aktif </span>
                                <?php } else { ?>
                                    <span class="label label-danger arrowed">Nonaktif</span>
                                <?php }  ?>
                            </td>
                            <td>
                                <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="black" href="<?= base_url('kcp/update/'); ?><?= $nsb['id_kcp'];  ?>" title="Update">
                                        <i class="ace-icon fa fa-edit bigger-130"></i>
                                    </a>

                                    <?php
                                    if ($nsb['aktif'] == 1) { ?>
                                        <a class="red" href="<?= base_url('kcp/nonaktif/'); ?><?= $nsb['id_kcp'];  ?>" title="Non Aktif">
                                            <i class="ace-icon fa fa-close bigger-130"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a class="green" href="<?= base_url('kcp/aktif/'); ?><?= $nsb['id_kcp'];  ?>" title="Aktif">
                                            <i class="ace-icon fa fa-check bigger-130"></i>
                                        </a>
                                    <?php }  ?>

                                </div>

                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
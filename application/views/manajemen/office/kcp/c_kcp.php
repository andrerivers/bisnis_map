<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= validation_errors(); ?>
                </div>
                <?php endif ?>
                <div class="widget-main">
                    <div class="row">
                        <form action="<?= base_url('kcp/create'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">
                            <div>
                                    <label for="form-field-8">Kantor Cabang</label>
                                    <select class="select2 form-control" id="kode_cabang"
                                        name="kode_cabang" data-placeholder="Choose a customer...">
                                        <option selected disabled value="">Pilih</option>
                                        <?php foreach ($kc as $bs) : ?>
                                        <option value="<?= $bs['kode_cabang']; ?>"><?= $bs['nama_cabang']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Kode KCP</label>
                                    <input type="text" class="form-control" id="kode_kcp" name="kode_kcp" value="<?= time() ?>" readonly required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Nama KCP</label>
                                    <input type="text" class="form-control" id="nama_kcp" name="nama_kcp" required>
                                </div>

                            </div>

                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Submit
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
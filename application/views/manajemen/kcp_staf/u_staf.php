<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-header">
                <h4 class="widget-title"><?= $title; ?></h4>

                <span class="widget-toolbar">
                    <a href="#" data-action="settings">
                        <i class="ace-icon fa fa-cog"></i>
                    </a>

                    <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>

                    <a href="#" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <a href="#" data-action="close">
                        <i class="ace-icon fa fa-times"></i>
                    </a>
                </span>
            </div>

            <div class="widget-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <?= validation_errors(); ?>
                    </div>
                <?php endif ?>
                <div class="widget-main">
                    <div class="row">
                        <form action="<?= base_url('staf/updateGo'); ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xs-6">
                                <div>
                                    <label for="form-field-8">ID USER</label>
                                    <input type="text" class="form-control" id="id_user" name="id_user" value="<?= $d['id_user']; ?>" readonly required>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Unit Kerja</label>
                                    <select class="selectpicker form-control" id="kode_kcp" name="kode_kcp" data-placeholder="Choose a customer...">
                                        <option selected disabled value="">Pilih</option>
                                        <?php foreach ($kcp as $x) : ?>
                                            <option <?php if ($x['kode_kcp'] == $d['kode_kcp']) {
                                                        echo 'selected="selected"';
                                                    } ?> value="<?= $x['kode_kcp']; ?>"><?= $x['nama_kcp']; ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">NIP</label>
                                    <input type="text" class="form-control" id="nip" name="nip" value="<?= $d['nip']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $d['nama']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tempat Lahir</label>
                                    <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?= $d['tempat_lahir']; ?>" required>
                                </div>


                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= $d['tgl_lahir']; ?>" required>
                                </div>


                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Jenis Kelamin</label>
                                    <select class="select2 form-control" id="jk" name="jk" data-placeholder="Choose a customer...">
                                        <option value="<?= $d['jk']; ?>"><?= $d['jk']; ?></option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">No. Hp</label>
                                    <input type="number" class="form-control" id="no_hp" name="no_hp" value="<?= $d['no_hp']; ?>" required>
                                </div>


                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="<?= $d['email']; ?>" required>
                                </div>

                                <div class="space-4"></div>
                                <div>
                                    <label for="form-field-8">Role ID</label>
                                    <select class="select2 form-control" id="role_id" name="role_id" data-placeholder="Choose a customer...">
                                        <option selected disabled value="">Pilih</option>
                                        <option value="1">Administrator</option>
                                        <option value="2">User</option>
                                    </select>
                                </div>
                            </div>

                    </div>
                    <div class="space-8"></div>
                    <button type="submit" class="btn btn-white btn-info btn-bold">
                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                        Submit
                    </button>
                    </form>
                </div>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
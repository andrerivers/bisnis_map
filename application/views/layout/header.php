<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title><?= $title; ?></title>
    <meta name="description" content="overview &amp; stats" />
    <link rel="shortcut icon" type="image/icon" href="<?= base_url('assets/img/icon.png'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/fonts.googleapis.com.css" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/ace-skins.min.css" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/jquery-ui.custom.min.css" />
    <link rel="stylesheet" href="<?= base_url('assets/thema/'); ?>assets/css/chosen.min.css" />
    <script src="<?= base_url('assets/thema/'); ?>assets/js/ace-extra.min.js"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('assets/') ?>sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

</head>

<body class="no-skin">
    <div id="navbar" class="navbar navbar-default ace-save-state">
        <div class="navbar-container ace-save-state" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                <span class="sr-only">Toggle sidebar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="navbar-header pull-left">
                <a href="#" class="navbar-brand">
                    <small>
                        Business Maping
                    </small>
                </a>
            </div>

            <div class="navbar-buttons navbar-header pull-right" role="navigation">
                <ul class="nav ace-nav">
                    <li class="light-orange dropdown-modal">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <img class="nav-user-photo" src="<?= base_url('assets/thema/'); ?>assets/images/avatars/user.jpg" alt="Jason's Photo" />
                            <span class="user-info">
                                <small>Welcome,</small>
                                <?php echo $this->session->userdata('nama'); ?>
                            </span>

                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>

                        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                            <li>
                                <a href="<?= base_url('sales/password'); ?>">
                                    <i class="ace-icon fa fa-cog"></i>
                                    Ubah Password
                                </a>
                            </li>

                            <li>
                                <a href="<?= base_url('sales/profil'); ?>">
                                    <i class="ace-icon fa fa-user"></i>
                                    Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="<?= base_url('auth/logout'); ?>">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div><!-- /.navbar-container -->
    </div>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
            try {
                ace.settings.loadState('main-container')
            } catch (e) {}
        </script>

        <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
            <script type="text/javascript">
                try {
                    ace.settings.loadState('sidebar')
                } catch (e) {}
            </script>
            <ul class="nav nav-list">
                <li class="<?= isset($active_menu_dashboard) ? $active_menu_dashboard : '' ?>">
                    <a href="<?= base_url('sales'); ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text"> Dashboard </span>
                    </a>
                    <b class="arrow"></b>
                </li>

                <li class="<?= isset($active_menu_maping) ? $active_menu_maping : '' ?>">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle <?= isset($active_menu_mp) ? $active_menu_mp : '' ?>">
                        <i class="menu-icon fa fa-building-o"></i>
                        <span class="menu-text">
                            Form Bisnis
                        </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <?php if ($this->session->userdata('role_id') == 2) { ?>
                            <li class="<?= isset($active_menu_form) ? $active_menu_form : '' ?>">
                                <a href="<?= base_url('company/form'); ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Tambah Data
                                </a>

                                <b class="arrow"></b>
                            </li>
                        <?php } ?>

                        <li class="<?= isset($active_menu_com_draft) ? $active_menu_com_draft : '' ?>">
                            <a href="<?= base_url('company/draft'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Pipline Draft
                            </a>

                            <b class="arrow"></b>
                        </li>


                        <li class="<?= isset($active_menu_com_hot) ? $active_menu_com_hot : '' ?>">
                            <a href="<?= base_url('company/hot'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Pipline Hot
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_com_cold) ? $active_menu_com_cold : '' ?>">
                            <a href="<?= base_url('company/cold'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Pipline Cold
                            </a>

                            <b class="arrow"></b>
                        </li>
                        <!-- 
                        <li class="<?= isset($active_menu_com_done) ? $active_menu_com_done : '' ?>">
                            <a href="<?= base_url('company/done'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Pipline Done
                            </a>

                            <b class="arrow"></b>
                        </li>
 -->

                    </ul>
                </li>


                <li class="<?= isset($active_menu_msystem) ? $active_menu_msystem : '' ?>">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle <?= isset($active_menu_ms) ? $active_menu_ms : '' ?>">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">
                            Form Perorangan
                        </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <?php if ($this->session->userdata('role_id') == 2) { ?>
                            <li class="<?= isset($active_menu_form) ? $active_menu_form : '' ?>">
                                <a href="<?= base_url('sales/nasabah'); ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Tambah Data
                                </a>

                                <b class="arrow"></b>
                            </li>
                        <?php } ?>
                        <li class="<?= isset($active_menu_pros) ? $active_menu_pros : '' ?>">
                            <a href="<?= base_url('prospecting/view'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Prospecting
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_qual) ? $active_menu_qual : '' ?>">
                            <a href="<?= base_url('qualification/view'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Qualification
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_mt) ? $active_menu_mt : '' ?>">
                            <a href="<?= base_url('meeting/view'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Meeting
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_pp) ? $active_menu_pp : '' ?>">
                            <a href="<?= base_url('proposal/view'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Proposal
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_cls) ? $active_menu_cls : '' ?>">
                            <a href="<?= base_url('closing/view'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Closing
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_rtt) ? $active_menu_rtt : '' ?>">
                            <a href="<?= base_url('retention/view'); ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Retention
                            </a>

                            <b class="arrow"></b>
                        </li>

                    </ul>
                </li>

                <?php if ($this->session->userdata('role_id') == 2) { ?>
                    <!-- SALES -->
                    <li class="<?= isset($active_menu_kalender) ? $active_menu_kalender : '' ?>">
                        <a href="<?= base_url('sales/maps'); ?>">
                            <i class="menu-icon fa fa-map-marker"></i>
                            <span class="menu-text">
                                Maps Perorangan
                            </span>
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="<?= isset($active_menu_kalender) ? $active_menu_kalender : '' ?>">
                        <a href="<?= base_url('sales/maps_bisnis'); ?>">
                            <i class="menu-icon fa fa-map-marker"></i>
                            <span class="menu-text">
                                Maps Bisnis
                            </span>
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?= isset($active_menu_report) ? $active_menu_report : '' ?>">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="menu-icon fa fa-print"></i>
                            <span class="menu-text"> Report </span>

                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">
                            <li class="<?= isset($active_menu_perorangan) ? $active_menu_perorangan : '' ?>">
                                <a href="<?= base_url('report/perorangan') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Perorangan
                                </a>

                                <b class="arrow"></b>
                            </li>

                            <li class="<?= isset($active_menu_upass) ? $active_menu_upass : '' ?>">
                                <a href="<?= base_url('report/perusahaan') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Perusahaan
                                </a>

                                <b class="arrow"></b>
                            </li>
                        </ul>
                    </li>
                <?php } ?>

                <?php if ($this->session->userdata('role_id') == 1) { ?>
                    <!-- SALES -->
                    <li class="<?= isset($active_menu_mapsPerorangan) ? $active_menu_mapsPerorangan : '' ?>">
                        <a href="<?= base_url('sales/maps'); ?>">
                            <i class="menu-icon fa fa-map-marker"></i>
                            <span class="menu-text">
                                Maps Perorangan
                            </span>
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?= isset($active_menu_mapsBisnis) ? $active_menu_mapsBisnis : '' ?>">
                        <a href="<?= base_url('sales/maps_bisnis'); ?>">
                            <i class="menu-icon fa fa-map-marker"></i>
                            <span class="menu-text">
                                Maps Bisnis
                            </span>
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="<?= isset($active_menu_report) ? $active_menu_report : '' ?>">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="menu-icon fa fa-print"></i>
                            <span class="menu-text"> Report </span>

                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">
                            <li class="<?= isset($active_menu_perorangan) ? $active_menu_perorangan : '' ?>">
                                <a href="<?= base_url('report/perorangan') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Perorangan
                                </a>

                                <b class="arrow"></b>
                            </li>

                            <li class="<?= isset($active_menu_upass) ? $active_menu_upass : '' ?>">
                                <a href="<?= base_url('report/perusahaan') ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Perusahaan
                                </a>

                                <b class="arrow"></b>
                            </li>
                        </ul>
                    </li>
                <?php } ?>


                <li class="<?= isset($active_menu_pengaturan) ? $active_menu_pengaturan : '' ?>">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        <i class="menu-icon fa fa-cog"></i>
                        <span class="menu-text"> Setting </span>

                        <b class="arrow fa fa-angle-down"></b>
                    </a>

                    <b class="arrow"></b>

                    <ul class="submenu">
                        <li class="<?= isset($active_menu_uprofil) ? $active_menu_uprofil : '' ?>">
                            <a href="<?= base_url('sales/profil') ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Profil
                            </a>

                            <b class="arrow"></b>
                        </li>

                        <li class="<?= isset($active_menu_upass) ? $active_menu_upass : '' ?>">
                            <a href="<?= base_url('sales/password') ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Change Password
                            </a>

                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <?php if ($this->session->userdata('role_id') == 1) { ?>
                    <li class="<?= isset($active_menu_man) ? $active_menu_man : '' ?>">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="menu-icon fa fa-desktop"></i>
                            <span class="menu-text">
                                Man. system
                            </span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">
                            <li class="<?= isset($active_menu_kantor) ? $active_menu_kantor : '' ?>">
                                <a href="#" class="dropdown-toggle">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Kantor
                                    <b class="arrow fa fa-angle-down"></b>
                                </a>

                                <b class="arrow"></b>

                                <ul class="submenu">
                                    <li class="<?= isset($active_menu_kc) ? $active_menu_kc : '' ?>">
                                        <a href="<?= base_url('kc/view'); ?>">
                                            <i class="menu-icon fa fa-caret-right"></i>
                                            Kantor Cabang
                                        </a>

                                        <b class="arrow"></b>
                                    </li>

                                    <li class="<?= isset($active_menu_kcp) ? $active_menu_kcp : '' ?>">
                                        <a href="<?= base_url('kcp/view'); ?>">
                                            <i class="menu-icon fa fa-caret-right"></i>
                                            Sub Kantor Cabang
                                        </a>

                                        <b class="arrow"></b>
                                    </li>
                                </ul>
                            </li>

                            <li class="<?= isset($active_menu_staf) ? $active_menu_staf : '' ?>">
                                <a href="<?= base_url('staf/view'); ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Staff
                                </a>

                                <b class="arrow"></b>
                            </li>


                        </ul>
                    </li>

                <?php } ?>


                <?php if ($this->session->userdata('role_id') == 3) { ?>
                    <li class="<?= isset($active_menu_man) ? $active_menu_man : '' ?>">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="menu-icon fa fa-desktop"></i>
                            <span class="menu-text">
                                Man. system
                            </span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>

                        <b class="arrow"></b>

                        <ul class="submenu">

                            <li class="<?= isset($active_menu_staf) ? $active_menu_staf : '' ?>">
                                <a href="<?= base_url('staf_cabang/view'); ?>">
                                    <i class="menu-icon fa fa-caret-right"></i>
                                    Staff
                                </a>

                                <b class="arrow"></b>
                            </li>


                        </ul>
                    </li>

                <?php } ?>

                <li class="<?= isset($active_menu_logout) ? $active_menu_logout : '' ?>">
                    <a href="<?= base_url('auth/logout') ?>">
                        <i class="menu-icon fa fa-power-off"></i>
                        <span class="menu-text"> Logout </span>
                    </a>

                    <b class="arrow"></b>
                </li>


            </ul><!-- /.nav-list -->

            <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
                <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
        </div>

        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="ace-icon fa fa-home home-icon"></i>
                            <a href="#">Home</a>
                        </li>
                        <li class="active"><?= $title; ?></li>
                    </ul><!-- /.breadcrumb -->
                </div>

                <div class="page-content">
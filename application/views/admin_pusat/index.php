<?php
$Prospecting = $this->db->query("SELECT * FROM dbm_nasabah where sts=1");
$Qualification = $this->db->query("SELECT * FROM dbm_nasabah where sts=2");
$Meeting = $this->db->query("SELECT * FROM dbm_nasabah where sts=3");
$Proposal = $this->db->query("SELECT * FROM dbm_nasabah where sts=4");
$Closing = $this->db->query("SELECT * FROM dbm_nasabah where sts=5");
$Retention = $this->db->query("SELECT * FROM dbm_nasabah where sts=6");

$Draft = $this->db->query("SELECT * FROM dbm_bisnis_map where sts_pipeline=0");
$Hot = $this->db->query("SELECT * FROM dbm_bisnis_map where sts_pipeline=1");
$Cold = $this->db->query("SELECT * FROM dbm_bisnis_map where sts_pipeline=2");
$Done = $this->db->query("SELECT * FROM dbm_bisnis_map where sts_pipeline=3");


?>

<div class="row">
    <div class="space-6"></div>
    <div class="col-sm-6 infobox-container">
        <h4 align="center">PERORANGAN</h4>
        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-comments"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Prospecting->num_rows(); ?></span>
                <div class="infobox-content">Prospecting</div>
            </div>


        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-twitter"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Qualification->num_rows(); ?></span>
                <div class="infobox-content">Qualification</div>
            </div>

        </div>

        <div class="infobox infobox-orange">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-shopping-cart"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Meeting->num_rows(); ?></span>
                <div class="infobox-content">Meeting</div>
            </div>
        </div>

        <div class="infobox infobox-red">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-flask"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Proposal->num_rows(); ?></span>
                <div class="infobox-content">Proposal</div>
            </div>
        </div>

        <div class="infobox infobox-black">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-flask"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Closing->num_rows(); ?></span>
                <div class="infobox-content">Closing</div>
            </div>
        </div>

        <div class="infobox infobox-purple">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-flask"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Retention->num_rows(); ?></span>
                <div class="infobox-content">Retention</div>
            </div>
        </div>

    </div>

    <div class="col-sm-6 infobox-container">
        <h4 align="center">PERUSAHAAN</h4>
        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-comments"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Draft->num_rows(); ?></span>
                <div class="infobox-content">Draft</div>
            </div>


        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-twitter"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Hot->num_rows(); ?></span>
                <div class="infobox-content">Hot</div>
            </div>

        </div>

        <div class="infobox infobox-orange">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-shopping-cart"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Cold->num_rows(); ?></span>
                <div class="infobox-content">Cold</div>
            </div>
        </div>

        <div class="infobox infobox-purple">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-flask"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $Done->num_rows(); ?></span>
                <div class="infobox-content">Done</div>
            </div>
        </div>



    </div>
</div>

<div class="space-10"></div>

<div class="col-sm-12">
    <div class="widget-box">
        <div class="widget-header widget-header-flat widget-header-small">
            <h5 class="widget-title">
                <i class="ace-icon fa fa-signal"></i>
                Traffic Pipeline
            </h5>

            <div class="widget-toolbar no-border">
                <div class="inline dropdown-hover">
                    <button class="btn btn-minier btn-primary">
                        This Week
                        <i class="ace-icon fa fa-angle-down icon-on-right bigger-110"></i>
                    </button>

                    <ul class="dropdown-menu dropdown-menu-right dropdown-125 dropdown-lighter dropdown-close dropdown-caret">
                        <li class="active">
                            <a href="#" class="blue">
                                <i class="ace-icon fa fa-caret-right bigger-110">&nbsp;</i>
                                This Week
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                Last Week
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                This Month
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="ace-icon fa fa-caret-right bigger-110 invisible">&nbsp;</i>
                                Last Month
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-main">
                <div id="piechart-placeholder"></div>

            </div><!-- /.widget-main -->
        </div><!-- /.widget-body -->
    </div><!-- /.widget-box -->
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
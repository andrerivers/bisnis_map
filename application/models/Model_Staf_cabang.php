<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_Staf_cabang extends CI_Model
{
    public function getUser()
    {
        $kode_kcp = $this->session->userdata('kode_kcp');
        $this->db->select('*');
        $this->db->from('dbm_user a');
        $this->db->join('dbm_kcp b', 'b.kode_kcp = a.kode_kcp');
        $this->db->where('a.kode_kcp', $kode_kcp);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateUser($data,  $id_user)
    {
        $this->db->where('id_user', $id_user);
        $res = $this->db->update('dbm_user', $data);
        return $res;
    }

    public function detailUser($id_user)
    {
        $this->db->select('*');
        $this->db->from('dbm_user a');
        $this->db->where('id_user', $id_user);
        $this->db->join('dbm_kcp b', 'b.kode_kcp = a.kode_kcp');
        $query = $this->db->get();
        return $query->row_array();
    }
}

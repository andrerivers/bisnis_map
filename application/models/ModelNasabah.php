<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ModelNasabah extends CI_Model

{
    public function getNasabah()

    {
        if ($this->session->userdata('role_id') == 2) {
            $username = $this->session->userdata('username');
            $this->db->select('*');
            $this->db->from('dbm_nasabah a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
            $this->db->where('a.username', $username);
            $query = $this->db->get();
            return $query->result_array();
        } elseif ($this->session->userdata('role_id') == 1) {
            $this->db->select('*');
            $this->db->from('dbm_nasabah a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
            $query = $this->db->get();
            return $query->result_array();
        } else {
            $kode_kcp = $this->session->userdata('kode_kcp');
            $this->db->select('*');
            $this->db->from('dbm_nasabah a');
            $this->db->join('dbm_user c', 'c.username = a.username');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
            $this->db->where('c.kode_kcp', $kode_kcp);
            $query = $this->db->get();
            return $query->result_array();
        }
    }


    public function getBisnis()

    {
        $this->db->select('*');
        $this->db->from('dbm_kategori_bisnis');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getJenisTab()

    {
        $this->db->select('*');
        $this->db->from('dbm_jenis_tabungan');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function detailNasabah($uid_nasabah)

    {
        $this->db->select('*');
        $this->db->select('a.nama AS nama_lengkap');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_nasabah a');
        $this->db->join('dbm_jenis_tabungan h', 'h.id_tab = a.id_tabungan');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $this->db->where('a.uid_nasabah', $uid_nasabah);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function updateNasabah($data, $uid_nasabah)
    {
        $this->db->where('uid_nasabah', $uid_nasabah);
        $res = $this->db->update('dbm_nasabah', $data);
        return $res;
    }


    public function detailCompany($uid_form)

    {
        $this->db->select('*');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_bisnis_map a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $this->db->where('a.uid_form', $uid_form);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getViewDraft()

    {
        if ($this->session->userdata('role_id') == 2) {
            $username = $this->session->userdata('username');
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->where('a.sts_pipeline', 0);
            $this->db->where('a.username', $username);
            $query = $this->db->get();
            return $query->result_array();
        } elseif ($this->session->userdata('role_id') == 1) {
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->where('a.sts_pipeline', 0);
            $query = $this->db->get();
            return $query->result_array();
        } else {
            $kode_kcp = $this->session->userdata('kode_kcp');
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->join('dbm_user z', 'z.username = a.username');
            $this->db->where('a.sts_pipeline', 0);
            $this->db->where('a.kode_kcp', $kode_kcp);
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    public function getDetailDraft($uid_form)

    {
        $username = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_bisnis_map a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $this->db->where('a.uid_form', $uid_form);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getViewHot()

    {
        if ($this->session->userdata('role_id') == 2) {
            $username = $this->session->userdata('username');
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->where('a.sts_pipeline', 1);
            $this->db->where('a.username', $username);
            $query = $this->db->get();
            return $query->result_array();
        } elseif ($this->session->userdata('role_id') == 1) {
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->where('a.sts_pipeline', 1);
            $query = $this->db->get();
            return $query->result_array();
        } else {
            $kode_kcp = $this->session->userdata('kode_kcp');
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->join('dbm_user z', 'z.username = a.username');
            $this->db->where('a.sts_pipeline', 1);
            $this->db->where('a.kode_kcp', $kode_kcp);
            $query = $this->db->get();
            return $query->result_array();
        }
    }

    public function getViewCold()

    {
        if ($this->session->userdata('role_id') == 2) {
            $username = $this->session->userdata('username');
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->where('a.sts_pipeline', 2);
            $this->db->where('a.username', $username);
            $query = $this->db->get();
            return $query->result_array();
        } elseif ($this->session->userdata('role_id') == 1) {
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->where('a.sts_pipeline', 2);
            $query = $this->db->get();
            return $query->result_array();
        } else {
            $kode_kcp = $this->session->userdata('kode_kcp');
            $this->db->select('*');
            $this->db->select('c.nama AS provinsi');
            $this->db->select('f.nama AS kota');
            $this->db->select('d.nama AS kecamatan');
            $this->db->select('e.nama AS kelurahan');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
            $this->db->join('t_kota f', 'f.id = a.id_kota');
            $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
            $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
            $this->db->join('dbm_user z', 'z.username = a.username');
            $this->db->where('a.sts_pipeline', 2);
            $this->db->where('a.kode_kcp', $kode_kcp);
            $query = $this->db->get();
            return $query->result_array();
        }
    }


    public function updateNasabahMap($data,  $uid_nasabah)
    {
        $this->db->where('uid_nasabah', $uid_nasabah);
        $res = $this->db->update('dbm_nasabah', $data);
        return $res;
    }


    public function updateBisnisMap($data,  $uid_form)
    {
        $this->db->where('uid_form', $uid_form);
        $res = $this->db->update('dbm_bisnis_map', $data);
        return $res;
    }



    public function updateBisnisForm_bisnis_pemerintah($data,  $uid_form)
    {
        $this->db->where('uid_form', $uid_form);
        $res = $this->db->update('dbm_bisnis_map', $data);
        return $res;
    }

    public function getProgress($uid_form)

    {
        $this->db->select('*');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_bisnis_map a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $this->db->join('dbm_history_map g', 'g.uid_form = a.uid_form');
        $this->db->where('a.uid_form', $uid_form);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function mapsNasabah()

    {
        $this->db->select('*');
        $this->db->select('a.nama AS nama_lengkap');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_nasabah a');
        $this->db->join('dbm_jenis_tabungan h', 'h.id_tab = a.id_tabungan');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getMapsBisnis()

    {
        if ($this->session->userdata('role_id') == 2) {
            $username = $this->session->userdata('username');
            $this->db->select('*');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->where('a.username', $username);
            $query = $this->db->get();
            return $query->result_array();
        } elseif ($this->session->userdata('role_id') == 1) {
            $this->db->select('*');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $query = $this->db->get();
            return $query->result_array();
        } else {
            $kode_kcp = $this->session->userdata('kode_kcp');
            $this->db->select('*');
            $this->db->from('dbm_bisnis_map a');
            $this->db->join('dbm_user c', 'c.username = a.username');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
            $this->db->where('c.kode_kcp', $kode_kcp);
            $query = $this->db->get();
            return $query->result_array();
        }
    }
}

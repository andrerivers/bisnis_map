<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_Bisnis_Pendidikan extends CI_Model

{

    public function update($data,  $uid_form)
    {
        $this->db->where('uid_form', $uid_form);
        $res = $this->db->update('dbm_bisnis_map', $data);
        return $res;
    }

    public function detail($uid_form)

    {
        $username = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_bisnis_map a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $this->db->where('a.uid_form', $uid_form);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getNasabah()

    {

        $username = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->from('dbm_nasabah a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
        $this->db->where('a.username', $username);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detailCompany($uid_form)

    {
        $this->db->select('*');
        $this->db->select('c.nama AS provinsi');
        $this->db->select('f.nama AS kota');
        $this->db->select('d.nama AS kecamatan');
        $this->db->select('e.nama AS kelurahan');
        $this->db->from('dbm_bisnis_map a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_kategori_bisnis');
        $this->db->join('t_provinsi c', 'c.id = a.id_provinsi');
        $this->db->join('t_kota f', 'f.id = a.id_kota');
        $this->db->join('t_kecamatan d', 'd.id = a.id_kecamatan');
        $this->db->join('t_kelurahan e', 'e.id = a.id_kelurahan');
        $this->db->where('a.uid_form', $uid_form);
        $query = $this->db->get();
        return $query->row_array();
    }
}

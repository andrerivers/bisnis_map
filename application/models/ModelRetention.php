<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelRetention extends CI_Model

{
    public function getRetention()

    {
        if ($this->session->userdata('role_id') == 2) {
            $username = $this->session->userdata('username');
            $this->db->select('*');
            $this->db->from('dbm_nasabah a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
            $this->db->where('a.sts', 6);
            $this->db->where('a.username', $username);
            $query = $this->db->get();
            return $query->result_array();
        } elseif ($this->session->userdata('role_id') == 1) {
            $this->db->select('*');
            $this->db->from('dbm_nasabah a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
            $this->db->where('a.sts', 6);
            $query = $this->db->get();
            return $query->result_array();
        } else {
            $kode_kcp = $this->session->userdata('kode_kcp');
            $this->db->select('*');
            $this->db->from('dbm_nasabah a');
            $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
            $this->db->join('dbm_user c', 'c.username = a.username');
            $this->db->where('a.sts', 6);
            $this->db->where('c.kode_kcp', $kode_kcp);
            $query = $this->db->get();
            return $query->result_array();
        }
    }


    public function getNasabah()

    {
        $username = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->from('dbm_nasabah a');
        $this->db->where('a.sts', 0);
        $this->db->where('a.username', $username);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSwitch()

    {
        $username = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->from('dbm_nasabah a');
        $this->db->where('a.sts', 6);
        $this->db->where('a.username', $username);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getBisnis()

    {
        $this->db->select('*');
        $this->db->from('dbm_kategori_bisnis');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getDetail($uid_nasabah)

    {
        $this->db->select('*');
        $this->db->from('dbm_nasabah a');
        $this->db->join('dbm_kategori_bisnis b', 'b.id_bisnis = a.id_bisnis');
        $this->db->where('a.uid_nasabah', $uid_nasabah);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getHistory($uid_nasabah)

    {
        $this->db->select('*');
        $this->db->from('dbm_history');
        $this->db->where('uid_nasabah', $uid_nasabah);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateNasabah($data, $uid_nasabah)
    {
        $this->db->where('uid_nasabah', $uid_nasabah);
        $res = $this->db->update('dbm_nasabah', $data);
        return $res;
    }
}

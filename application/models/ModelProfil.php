<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ModelProfil extends CI_Model
{
    public function getUser()
    {
        $this->db->select('*');
        $this->db->from('dbm_user a');
        $this->db->join('dbm_kcp b', 'b.kode_kcp = a.kode_kcp');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateProfil($data,  $id_user)
    {
        $this->db->where('id_user', $id_user);
        $res = $this->db->update('dbm_user', $data);
        return $res;
    }

    public function detailProfil()
    {
        $username = $this->session->userdata('username');
        $this->db->select('*');
        $this->db->from('dbm_user a');
        $this->db->join('dbm_kcp b', 'b.kode_kcp = a.kode_kcp');
        $this->db->where('username', $username);
        $query = $this->db->get();
        return $query->row_array();
    }
}

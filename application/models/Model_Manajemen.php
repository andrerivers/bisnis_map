<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Model_Manajemen extends CI_Model
{
    public function getKc()
    {
        $this->db->select('*');
        $this->db->from('dbm_kc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateKc($data,  $id_kc)
    {
        $this->db->where('id_kc', $id_kc);
        $res = $this->db->update('dbm_kc', $data);
        return $res;
    }

    public function getDetailKc($id_kc)
    {
        $this->db->select('*');
        $this->db->from('dbm_kc');
        $this->db->where('id_kc', $id_kc);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getKcp()
    {
        $this->db->select('*');
        $this->db->from('dbm_kc a');
        $this->db->join('dbm_kcp b', 'b.kode_cabang = a.kode_cabang');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    public function updateKcp($data,  $id_kcp)
    {
        $this->db->where('id_kcp', $id_kcp);
        $res = $this->db->update('dbm_kcp', $data);
        return $res;
    }

    public function getDetailKcp($id_kcp)
    {
        $this->db->select('*');
        $this->db->from('dbm_kcp');
        $this->db->where('id_kcp', $id_kcp);
        $query = $this->db->get();
        return $query->row_array();
    }


   
}

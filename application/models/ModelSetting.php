<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelSetting extends CI_Model
{

    public function list_setting()
    {
        $query = $this->db->get('dbm_setting');
        return $query->row();
    }

    public function edit($data)
    {
        $this->db->where('id_setting', $data['id_setting']);
        $this->db->update('dbm_setting', $data);
    }
}

/* End of file M_setting.php */
/* Location: ./application/models/M_setting.php */

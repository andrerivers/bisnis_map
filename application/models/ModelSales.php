<?php

defined('BASEPATH') or exit('No direct script access allowed');



class ModelSales extends CI_Model

{

    public function getBisnis()

    {
        $this->db->select('*');
        $this->db->from('dbm_kategori_bisnis');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDpk()

    {
        $this->db->select('*');
        $this->db->from('dbm_dpk');
        $query = $this->db->get();
        return $query->result_array();
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('ModelSales', 'ms');
        $this->load->model('ModelNasabah', 'mn');
    }



    public function form()
    {
        $data = array(
            'title' => 'Form Bisnis',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'nasabah' => $this->mn->getNasabah()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/form', $data);
        $this->load->view('layout/footer');
    }


    public function view_draft()
    {
        $data = array(
            'title' => 'Form Bisnis',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_draft' => 'active',
            'bm' => $this->mn->getViewDraft()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/view', $data);
        $this->load->view('layout/footer');
    }


    public function view_hot()
    {
        $data = array(
            'title' => 'Form Bisnis',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_hot' => 'active',
            'bm' => $this->mn->getViewHot()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/view_hot', $data);
        $this->load->view('layout/footer');
    }

    public function view_cold()
    {
        $data = array(
            'title' => 'Form Bisnis',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_cold' => 'active',
            'bm' => $this->mn->getViewCold()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/view_cold', $data);
        $this->load->view('layout/footer');
    }

    public function viewProgress()
    {
        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Progress',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_draft' => 'active',
            'progress' => $this->mn->getProgress($uid_form)
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/v_progress', $data);
        $this->load->view('layout/footer');
    }


    public function createProgress()
    {
        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Progress',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_draft' => 'active',
            'd' => $this->mn->getDetailDraft($uid_form)
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/c_progress', $data);
        $this->load->view('layout/footer');
    }


    public function updateProgressGo()
    {

        $uid_form = htmlspecialchars($this->input->post('uid_form', true));
        $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));
        $pic = $this->session->userdata('username');

        $data = [
            'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
            'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
            'date_created' => date('Y-m-d H:i:s')
        ];

        $history = [
            'uid_form' => $uid_form,
            'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
            'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $log = [
            'log' => "Membuat Menambahkan Nasabah dengan $nama_instansi",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mn->updateBisnisForm_bisnis_pemerintah($data,  $uid_form);
        $this->db->insert('dbm_history_map', $history);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('company/viewProgress/' . $uid_form);
    }
}

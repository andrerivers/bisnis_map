<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nasabah extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('ModelSales', 'ms');
        $this->load->model('ModelNasabah', 'mn');
        $this->load->library(array("googlemaps"));
        $this->load->model('ModelSetting', 'mset');
    }



    public function v_nasabah()
    {
        $data = array(
            'title' => 'Customer',
            'active_menu_nasabah' => 'active',
            'nasabah' => $this->mn->getNasabah()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/v_nasabah', $data);
        $this->load->view('layout/footer');
    }

    public function c_nasabah()
    {
        $data = array(
            'title' => 'Create Customer',
            'active_menu_nasabah' => 'active',
            'nasabah' => $this->mn->getNasabah(),
            'bisnis' => $this->mn->getBisnis(),
            'jt' => $this->mn->getJenisTab()
        );
        $setting = $this->mset->list_setting();
        $this->load->library('googlemaps');
        $config['center'] = "$setting->latitude, $setting->longitude";
        $config['zoom'] = "$setting->zoom";
        $this->googlemaps->initialize($config);

        $marker['position'] = "$setting->latitude, $setting->longitude";
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setMapToForm(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);

        $map = $this->googlemaps->create_map();

        $this->form_validation->set_rules('no_identitas', 'no_identitas', 'trim|required|is_unique[dbm_nasabah.no_identitas]', [
            'is_unique' => 'Maaf no_identitas, Sudah Terdaftar!'
        ]);

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('nasabah/c_nasabah', $data);
            $this->load->view('layout/footer');
        } else {
            $pic = $this->session->userdata('username');
            $no_identitas = htmlspecialchars($this->input->post('no_identitas', true));
            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $data = [
                'uid_nasabah' => $reff,
                'no_identitas' => $no_identitas,
                'jenis_identitas' => htmlspecialchars($this->input->post('jenis_identitas', true)),
                'nama' => htmlspecialchars($this->input->post('nama', true)),
                'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
                'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
                'jk' => htmlspecialchars($this->input->post('jk', true)),
                'alamat' => htmlspecialchars($this->input->post('alamat', true)),
                'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
                'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
                'id_kelurahan' => htmlspecialchars($this->input->post('id_kelurahan', true)),
                'id_kecamatan' => htmlspecialchars($this->input->post('id_kecamatan', true)),
                'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
                'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'nama_alias' => htmlspecialchars($this->input->post('nama_alias', true)),
                'kewarganegaraan' => htmlspecialchars($this->input->post('kewarganegaraan', true)),
                'sts_kependudukan' => htmlspecialchars($this->input->post('sts_kependudukan', true)),
                'npwp' => htmlspecialchars($this->input->post('npwp', true)),
                'sts_perkawinan' => htmlspecialchars($this->input->post('sts_perkawinan', true)),
                'agama' => htmlspecialchars($this->input->post('agama', true)),
                'pendidikan' => htmlspecialchars($this->input->post('pendidikan', true)),
                'sts_tempat_tinggal' => htmlspecialchars($this->input->post('sts_tempat_tinggal', true)),
                'nama_usaha' => htmlspecialchars($this->input->post('nama_usaha', true)),
                'jabatan' => htmlspecialchars($this->input->post('jabatan', true)),
                'divisi' => htmlspecialchars($this->input->post('divisi', true)),
                'tgl_mulai_bekerja' => htmlspecialchars($this->input->post('tgl_mulai_bekerja', true)),
                'sumber_pendapatan' => htmlspecialchars($this->input->post('sumber_pendapatan', true)),
                'pendapatan' => htmlspecialchars($this->input->post('pendapatan', true)),
                'sts_pekerjaan' => htmlspecialchars($this->input->post('sts_pekerjaan', true)),
                'jenis_nasabah' => htmlspecialchars($this->input->post('jenis_nasabah', true)),
                'no_rekening' => htmlspecialchars($this->input->post('no_rekening', true)),
                'id_tabungan' => htmlspecialchars($this->input->post('id_tabungan', true)),
                'id_bisnis' => htmlspecialchars($this->input->post('id_bisnis', true)),
                'pekerjaan_sekarang' => htmlspecialchars($this->input->post('pekerjaan_sekarang', true)),
                'jenis_nasabah_lain' => htmlspecialchars($this->input->post('jenis_nasabah_lain', true)),
                'no_rekening_lain' => htmlspecialchars($this->input->post('no_rekening', true)),
                'date_created' => date('Y-m-d H:i:s'),
                'username' => $pic,
                'sts' => 0,
                'date_created' => date('Y-m-d H:i:s')
            ];

            $log = [
                'log' => "Membuat Menambahkan Nasabah dengan $reff",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('dbm_nasabah', $data);
            $this->db->insert('dbm_log', $log);

            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('sales/nasabah/maping/' . $reff);
        }
    }


    public function d_nasabah()
    {
        $uid_nasabah = $this->uri->segment(4);
        $data = array(
            'title' => 'Detail Customer',
            'active_menu_nasabah' => 'active',
            'd' => $this->mn->detailNasabah($uid_nasabah)

        );
        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/d_newnasabah', $data);
        $this->load->view('layout/footer');
    }

    public function u_nasabah()
    {
        $uid_nasabah = $this->uri->segment(4);
        $data = array(
            'title' => 'Update Customer',
            'active_menu_nasabah' => 'active',
            'd' => $this->mn->detailNasabah($uid_nasabah),
            'bisnis' => $this->mn->getBisnis(),
            'jt' => $this->mn->getJenisTab(),

        );
        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/u_nasabah', $data);
        $this->load->view('layout/footer');
    }


    public function updateGo()
    {
        $uid_nasabah = htmlspecialchars($this->input->post('uid_nasabah', true));
        $pic = $this->session->userdata('username');
        $no_identitas = htmlspecialchars($this->input->post('no_identitas', true));
        $data = [
            'no_identitas' => $no_identitas,
            'jenis_identitas' => htmlspecialchars($this->input->post('jenis_identitas', true)),
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
            'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
            'jk' => htmlspecialchars($this->input->post('jk', true)),
            'alamat' => htmlspecialchars($this->input->post('alamat', true)),
            'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
            'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
            'id_kelurahan' => htmlspecialchars($this->input->post('id_kelurahan', true)),
            'id_kecamatan' => htmlspecialchars($this->input->post('id_kecamatan', true)),
            'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
            'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'nama_alias' => htmlspecialchars($this->input->post('nama_alias', true)),
            'kewarganegaraan' => htmlspecialchars($this->input->post('kewarganegaraan', true)),
            'sts_kependudukkan' => htmlspecialchars($this->input->post('sts_kependudukkan', true)),
            'npwp' => htmlspecialchars($this->input->post('npwp', true)),
            'sts_perkawinan' => htmlspecialchars($this->input->post('sts_perkawinan', true)),
            'agama' => htmlspecialchars($this->input->post('agama', true)),
            'pendidikan' => htmlspecialchars($this->input->post('pendidikan', true)),
            'sts_tempat_tinggal' => htmlspecialchars($this->input->post('sts_tempat_tinggal', true)),
            'nama_usaha' => htmlspecialchars($this->input->post('nama_usaha', true)),
            'jabatan' => htmlspecialchars($this->input->post('jabatan', true)),
            'divisi' => htmlspecialchars($this->input->post('divisi', true)),
            'tgl_mulai_bekerja' => htmlspecialchars($this->input->post('tgl_mulai_bekerja', true)),
            'sumber_pendapatan' => htmlspecialchars($this->input->post('sumber_pendapatan', true)),
            'pendapatan' => htmlspecialchars($this->input->post('pendapatan', true)),
            'sts_pekerjaan' => htmlspecialchars($this->input->post('sts_pekerjaan', true)),
            'jenis_nasabah' => htmlspecialchars($this->input->post('jenis_nasabah', true)),
            'pekerjaan_sekarang' => htmlspecialchars($this->input->post('pekerjaan_sekarang', true)),
            'no_rekening' => htmlspecialchars($this->input->post('no_rekening', true)),
            'id_tabungan' => htmlspecialchars($this->input->post('id_tabungan', true)),
            'jenis_nasabah_lain' => htmlspecialchars($this->input->post('jenis_nasabah_lain', true)),
            'no_rekening_lain' => htmlspecialchars($this->input->post('no_rekening', true)),
            'id_bisnis' => htmlspecialchars($this->input->post('id_bisnis', true)),
            'username' => $pic
        ];

        $log = [
            'log' => "Mengupdate Nasabah dengan  $uid_nasabah",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mn->updateBisnisForm_bisnis_pemerintah($data,  $uid_nasabah);
        $this->db->insert('dbm_log', $log);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('sales/nasabah');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('sales/nasabah/update/' . $uid_nasabah);
        }
    }

    public function maping()
    {
        $uid_nasabah = $this->uri->segment(4);
        $setting = $this->mset->list_setting();
        $this->load->library('Googlemaps');
        $config['center'] = "$setting->latitude, $setting->longitude";
        $config['zoom'] = "$setting->zoom";
        $this->googlemaps->initialize($config);

        $marker['position'] = "$setting->latitude, $setting->longitude";
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setMapToForm(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $map = $this->googlemaps->create_map();
        $data = array(
            'title' => 'Maping Nasabah',
            'active_menu_nasabah' => 'active',
            'd' => $this->mn->detailNasabah($uid_nasabah),
            'bisnis' => $this->mn->getBisnis(),
            'jt' => $this->mn->getJenisTab(),
            'map' => $map,

        );

        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/c_maping', $data);
        $this->load->view('layout/footer');
    }



    public function updateMap()
    {
        $uid_nasabah = htmlspecialchars($this->input->post('uid_nasabah', true));
        $jenis_nasabah = htmlspecialchars($this->input->post('jenis_nasabah', true));
        if ($jenis_nasabah == "YES") {
            $icon = "yesNasabah.png";
        } else {
            $icon = "noNasabah.png";
        }
        $data = [
            'latitude' => htmlspecialchars($this->input->post('latitude', true)),
            'longitude' => htmlspecialchars($this->input->post('longitude', true)),
            'icon' => $icon
        ];

        $log = [
            'log' => "Mengupdate Map dengan  $uid_nasabah",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mn->updateNasabahMap($data,  $uid_nasabah);
        $this->db->insert('dbm_log', $log);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('sales/nasabah');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('sales/nasabah/update/' . $uid_nasabah);
        }
    }



    public function maping_bisnis()
    {
        $uid_form = $this->uri->segment(3);
        $setting = $this->mset->list_setting();
        $this->load->library('Googlemaps');
        $config['center'] = "$setting->latitude, $setting->longitude";
        $config['zoom'] = "$setting->zoom";
        $this->googlemaps->initialize($config);

        $marker['position'] = "$setting->latitude, $setting->longitude";
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setMapToForm(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $map = $this->googlemaps->create_map();
        $data = array(
            'title' => 'Maping Nasabah',
            'active_menu_nasabah' => 'active',
            'd' => $this->mn->detailCompany($uid_form),
            'bisnis' => $this->mn->getBisnis(),
            'jt' => $this->mn->getJenisTab(),
            'map' => $map,

        );

        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/c_maping_bisnis', $data);
        $this->load->view('layout/footer');
    }


    public function maping_bisnisGo()
    {
        $uid_form = htmlspecialchars($this->input->post('uid_form', true));
        $pgl_bank_sumut = htmlspecialchars($this->input->post('pgl_bank_sumut', true));

        if ($pgl_bank_sumut == "YES") {
            $icon = "yesNasabah.png";
        } else {
            $icon = "noNasabah.png";
        }
        $data = [
            'pgl_bank_sumut' => $pgl_bank_sumut,
            'latitude' => htmlspecialchars($this->input->post('latitude', true)),
            'longitude' => htmlspecialchars($this->input->post('longitude', true)),
            'icon' => $icon
        ];

        $log = [
            'log' => "Mengupdate Map dengan  $uid_form",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mn->updateBisnisMap($data,  $uid_form);
        $this->db->insert('dbm_log', $log);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('sales/maps_bisnis');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('sales/nasabah/buat_maps/' . $uid_form);
        }
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Qualification extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('ModelSales', 'ms');
        $this->load->model('ModelNasabah', 'mn');
        $this->load->model('ModelQualification', 'mp');
    }



    public function view()
    {
        $data = array(
            'title' => 'Pipeline - Qualification',
            'active_menu_msystem' => 'open',
            'active_menu_qual' => 'active',
            'nasabah' => $this->mn->getNasabah(),
            'v' => $this->mp->getQualification(),

        );
        $this->load->view('layout/header', $data);
        $this->load->view('pipeline/qualification/view', $data);
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $data = array(
            'title' => 'Create Pipeline qualification ',
            'active_menu_msystem' => 'open',
            'active_menu_qual' => 'active',
            'nasabah' => $this->mp->getNasabah(),
            'bisnis' => $this->mn->getBisnis(),
        );

        $this->form_validation->set_rules('due_date', 'Due Date', 'trim|required');
        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('pipeline/qualification/create', $data);
            $this->load->view('layout/footer');
        } else {

            $uid_nasabah = htmlspecialchars($this->input->post('uid_nasabah', true));
            $pic = $this->session->userdata('username');
            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $data = [
                'due_date' => htmlspecialchars($this->input->post('due_date', true)),
                'ket_lain' => htmlspecialchars($this->input->post('ket_lain', true)),
                'sts' => 2
            ];


            $history = [
                'uid_maping' => $reff,
                'uid_nasabah' => htmlspecialchars($this->input->post('uid_nasabah', true)),
                'due_date' => htmlspecialchars($this->input->post('due_date', true)),
                'ket_lain' => htmlspecialchars($this->input->post('ket_lain', true)),
                'username' => $pic,
                'sts' => 2,
                'date_created' => date('Y-m-d H:i:s')
            ];

            $log = [
                'log' => "Membuat Pipline qualification dengan $uid_nasabah",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $result = $this->mn->updateNasabah($data,  $uid_nasabah);
            $this->db->insert('dbm_history', $history);
            $this->db->insert('dbm_log', $log);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('qualification/view');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('qualification/view');
            }
        }
    }


    public function detail()
    {
        $uid_nasabah = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Data',
            'active_menu_msystem' => 'open',
            'active_menu_qual' => 'active',
            'prs' => $this->mp->getHistory($uid_nasabah),
            'bisnis' => $this->mn->getBisnis(),

        );
        $this->load->view('layout/header', $data);
        $this->load->view('pipeline/qualification/detail', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {
        $uid_nasabah = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Data',
            'active_menu_msystem' => 'open',
            'active_menu_qual' => 'active',
            'd' => $this->mp->getDetail($uid_nasabah),
            'bisnis' => $this->mn->getBisnis(),

        );
        $this->load->view('layout/header', $data);
        $this->load->view('pipeline/qualification/update', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {
        $uid_nasabah = htmlspecialchars($this->input->post('uid_nasabah', true));
        $pic = $this->session->userdata('username');
        $data = [
                'ket_lain' => htmlspecialchars($this->input->post('ket_lain', true)),
                'due_date' => htmlspecialchars($this->input->post('due_date', true)),
        ];

        $log = [
            'log' => "Mengupdate qualification dengan $uid_nasabah",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        
        $history = [
            'uid_maping' => $reff,
            'uid_nasabah' => htmlspecialchars($this->input->post('uid_nasabah', true)),
            'due_date' => htmlspecialchars($this->input->post('due_date', true)),
            'ket_lain' => htmlspecialchars($this->input->post('ket_lain', true)),
            'username' => $pic,
            'sts' => 2,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mn->updateNasabah($data,  $uid_nasabah);
        $this->db->insert('dbm_history', $history);
        $this->db->insert('dbm_log', $log);

        if ($result >= 1) {
            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('qualification/view');
        } else {
            $this->session->set_flashdata('gagal', 'Disimpan');
            redirect('qualification/view');
        }
    }


    public function switch()
    {
        $data = array(
            'title' => 'Switch Pipeline qualification ',
            'active_menu_msystem' => 'open',
            'active_menu_ms' => 'active',
            'active_menu_qual' => 'active',
            'nasabah' => $this->mp->getSwitch(),
            'bisnis' => $this->mn->getBisnis(),
        );

        $this->form_validation->set_rules('due_date', 'Due Date', 'trim|required');
        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('pipeline/qualification/switch', $data);
            $this->load->view('layout/footer');
        } else {

            $uid_nasabah = htmlspecialchars($this->input->post('uid_nasabah', true));
            $sts = htmlspecialchars($this->input->post('sts', true));
            $pic = $this->session->userdata('username');
            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $data = [
                'due_date' => htmlspecialchars($this->input->post('due_date', true)),
                'ket_lain' => htmlspecialchars($this->input->post('ket_lain', true)),
                'sts' => $sts
            ];


            $history = [
                'uid_maping' => $reff,
                'uid_nasabah' => htmlspecialchars($this->input->post('uid_nasabah', true)),
                'due_date' => htmlspecialchars($this->input->post('due_date', true)),
                'ket_lain' => htmlspecialchars($this->input->post('ket_lain', true)),
                'username' => $pic,
                'sts' => $sts,
                'date_created' => date('Y-m-d H:i:s')
            ];

            $log = [
                'log' => "Switch Pipline qualification dengan $uid_nasabah",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $result = $this->mn->updateNasabah($data,  $uid_nasabah);
            $this->db->insert('dbm_history', $history);
            $this->db->insert('dbm_log', $log);

            if ($result >= 1) {
                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('qualification/view');
            } else {
                $this->session->set_flashdata('gagal', 'Disimpan');
                redirect('qualification/view');
            }
        }
    }

}
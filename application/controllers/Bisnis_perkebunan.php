
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bisnis_perkebunan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('ModelSales', 'ms');
        $this->load->model('ModelNasabah', 'mn');
        $this->load->model('Model_Bisnis_Perkebunan', 'mbp');
    }

    public function form()
    {
        $data = array(
            'title' => 'Form Bisnis Perkebunan/Peternakan/DLL',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'nasabah' => $this->mbp->getNasabah()
        );
        $this->form_validation->set_rules('nama_instansi', 'Nama Instansi', 'trim|required');
        $this->form_validation->set_rules('alamat_instansi', 'Alamat Instansi', 'trim|required');
        $this->form_validation->set_rules('id_provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_kota', 'Kota', 'trim|required');
        $this->form_validation->set_rules('id_kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('id_kelurahan', 'kelurahan', 'trim|required');
        $this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|required');
        $this->form_validation->set_rules('no_hp_instansi', 'No Hp', 'trim|required');
        $this->form_validation->set_rules('email_instansi', 'Email Instansi', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('company/bisnis_perkebunan/create', $data);
            $this->load->view('layout/footer');
        } else {
            $pic = $this->session->userdata('username');
            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));

            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['dbm_bisnis_map']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $upload_image = $_FILES['foto']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto')) {
                    $old_image = $data['dbm_bisnis_map']['foto'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/' . $old_image);
                    }
                    $uploadfoto = $this->upload->data('file_name');
                    $this->db->set('foto', $uploadfoto);
                } else {
                    echo $this->upload->display_errors();
                }
            }
            $data = [
                'uid_form' => $reff,
                'nama_instansi' => htmlspecialchars($this->input->post('nama_instansi', true)),
                'sektor_usaha' => htmlspecialchars($this->input->post('sektor_usaha', true)),
                'alamat_instansi' => htmlspecialchars($this->input->post('alamat_instansi', true)),
                'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
                'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
                'id_kecamatan' => htmlspecialchars($this->input->post('id_kecamatan', true)),
                'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
                'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
                'id_kelurahan' => htmlspecialchars($this->input->post('id_kelurahan', true)),
                'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
                'no_hp_instansi' => htmlspecialchars($this->input->post('no_hp_instansi', true)),
                'email_instansi' => htmlspecialchars($this->input->post('email_instansi', true)),
                'jumlah_pengusaha' => htmlspecialchars($this->input->post('jumlah_pengusaha', true)),
                'jumlah_transaksi' => htmlspecialchars($this->input->post('jumlah_transaksi', true)),
                'pgl_bank_sumut' => htmlspecialchars($this->input->post('pgl_bank_sumut', true)),
                'pgl_bank_lain' => htmlspecialchars($this->input->post('pgl_bank_lain', true)),
                'dpk_noa' => htmlspecialchars($this->input->post('dpk_noa', true)),
                'dpk_nominal' => htmlspecialchars($this->input->post('dpk_nominal', true)),
                'kredit_noa' => htmlspecialchars($this->input->post('kredit_noa', true)),
                'kredit_nominal' => htmlspecialchars($this->input->post('kredit_nominal', true)),
                'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'link_maps' => htmlspecialchars($this->input->post('link_maps', true)),
                'date_created' => date('Y-m-d H:i:s'),
                'username' => $pic,
                'sts' => 0,
                'file' => $upload1,
                'foto' => $uploadfoto,
                'id_kategori_bisnis' => 25,
                'date_created' => date('Y-m-d H:i:s')
            ];


            $history = [
                'uid_form' => $reff,
                'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
                'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];


            $log = [
                'log' => "Membuat Menambahkan Nasabah dengan $nama_instansi",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('dbm_bisnis_map', $data);
            $this->db->insert('dbm_history_map', $history);
            $this->db->insert('dbm_log', $log);

            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('bisnis_perkebunan/detail/' . $reff);
        }
    }

    public function detail()
    {
        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Bisnis Perkebunan/Perikanan/DLL',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'd' => $this->mbp->detailCompany($uid_form)

        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/bisnis_perkebunan/detail', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {

        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Bisnis bumn ',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_draft' => 'active',
            'd' => $this->mbp->detail($uid_form)
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/bisnis_perkebunan/update', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {

        $data = array(
            'title' => 'Form Bisnis bumn',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'nasabah' => $this->mbp->getNasabah()

        );
        $uid_form = htmlspecialchars($this->input->post('uid_form', true));
        $pic = $this->session->userdata('username');
        $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));

        $data2['detail'] = $this->db->get_where('dbm_bisnis_map',  ['uid_form' => $uid_form])->row_array();

        $uid_form = htmlspecialchars($this->input->post('uid_form', true));
        $pic = $this->session->userdata('username');
        $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data2['dbm_bisnis_map']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $upload_image = $_FILES['foto']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {
                $old_image = $data2['dbm_bisnis_map']['foto'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/' . $old_image);
                }
                $uploadfoto = $this->upload->data('file_name');
                $this->db->set('foto', $uploadfoto);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = [
            'nama_instansi' => htmlspecialchars($this->input->post('nama_instansi', true)),
            'sektor_usaha' => htmlspecialchars($this->input->post('sektor_usaha', true)),
            'alamat_instansi' => htmlspecialchars($this->input->post('alamat_instansi', true)),
            'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
            'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
            'id_kecamatan' => htmlspecialchars($this->input->post('id_kecamatan', true)),
            'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
            'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
            'id_kelurahan' => htmlspecialchars($this->input->post('id_kelurahan', true)),
            'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
            'no_hp_instansi' => htmlspecialchars($this->input->post('no_hp_instansi', true)),
            'email_instansi' => htmlspecialchars($this->input->post('email_instansi', true)),
            'jumlah_pengusaha' => htmlspecialchars($this->input->post('jumlah_pengusaha', true)),
            'jumlah_transaksi' => htmlspecialchars($this->input->post('jumlah_transaksi', true)),
            'pgl_bank_sumut' => htmlspecialchars($this->input->post('pgl_bank_sumut', true)),
            'pgl_bank_lain' => htmlspecialchars($this->input->post('pgl_bank_lain', true)),
            'dpk_noa' => htmlspecialchars($this->input->post('dpk_noa', true)),
            'dpk_nominal' => htmlspecialchars($this->input->post('dpk_nominal', true)),
            'kredit_noa' => htmlspecialchars($this->input->post('kredit_noa', true)),
            'kredit_nominal' => htmlspecialchars($this->input->post('kredit_nominal', true)),
            'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
            'catatan' => htmlspecialchars($this->input->post('catatan', true)),
            'link_maps' => htmlspecialchars($this->input->post('link_maps', true)),
            'username' => $pic,
            'sts' => 0,
            'file' => $upload1,
            'foto' => $uploadfoto,
            'id_kategori_bisnis' => 25,
        ];


        $history = [
            'uid_form' => $uid_form,
            'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
            'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];


        $log = [
            'log' => "Membuat Menambahkan Nasabah dengan $nama_instansi",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mbp->update($data,  $uid_form);
        $this->db->insert('dbm_history_map', $history);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('bisnis_perkebunan/detail/' . $uid_form);
    }
}

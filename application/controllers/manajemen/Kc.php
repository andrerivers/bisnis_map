
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kc extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('Model_Manajemen', 'mm');
    }

    public function view()
    {

        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Data Cabang',
            'active_menu_man' => 'open',
            'active_menu_kantor' => 'open',
            'active_menu_kc' => 'active',
            'kc' => $this->mm->getKc()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/office/kc/v_kc', $data);
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $data = array(
            'title' => 'Form Cabang',
            'active_menu_man' => 'open',
            'active_menu_kantor' => 'open',
            'active_menu_kc' => 'active',
        );

        $this->form_validation->set_rules('kode_cabang', 'Kode cabang', 'trim|required');
        $this->form_validation->set_rules('nama_cabang', 'Nama_cabang', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('manajemen/office/kc/c_kc', $data);
            $this->load->view('layout/footer');
        } else {
            $pic = $this->session->userdata('username');
            $data = [
                'kode_cabang' => htmlspecialchars($this->input->post('kode_cabang', true)),
                'nama_cabang' => htmlspecialchars($this->input->post('nama_cabang', true)),
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')
            ];


            $log = [
                'log' => "Menambahkan Data Cabang ",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('dbm_kc', $data);
            $this->db->insert('dbm_log', $log);

            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('kc/view');
        }
    }

    public function update()
    {

        $id_kc = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Cabang',
            'active_menu_man' => 'open',
            'active_menu_kantor' => 'open',
            'active_menu_kc' => 'active',
            'd' => $this->mm->getDetailKc($id_kc)
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/office/kc/u_kc', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {
       
        $pic = $this->session->userdata('username');
        $id_kc = htmlspecialchars($this->input->post('id_kc', true));
        $data = [
            'kode_cabang' => htmlspecialchars($this->input->post('kode_cabang', true)),
            'nama_cabang' => htmlspecialchars($this->input->post('nama_cabang', true)),
        ];

        $log = [
            'log' => "Merubah data Cabang $id_kc",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateKc($data,  $id_kc);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kc/view');
    }


    public function aktif()
    {
        $id_kc = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'aktif' => 1,
        ];

        $log = [
            'log' => "Merubah data Cabang $id_kc",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateKc($data,  $id_kc);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kc/view');
    }

    
    public function nonaktif()
    {
        $id_kc = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'aktif' => 0,
        ];

        $log = [
            'log' => "Merubah data Cabang $id_kc",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateKc($data,  $id_kc);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kc/view');
    }




}

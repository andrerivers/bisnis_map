
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kcp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('Model_Manajemen', 'mm');
    }

    public function view()
    {

        $data = array(
            'title' => 'Data Cabang',
            'active_menu_man' => 'open',
            'active_menu_kantor' => 'open',
            'active_menu_kcp' => 'active',
            'kc' => $this->mm->getKcp()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/office/kcp/v_kcp', $data);
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $data = array(
            'title' => 'Form Cabang',
            'active_menu_man' => 'open',
            'active_menu_kantor' => 'open',
            'active_menu_kcp' => 'active',
            'kc' => $this->mm->getKc()
        );

        $this->form_validation->set_rules('kode_cabang', 'Kode cabang', 'trim|required');
        $this->form_validation->set_rules('kode_kcp', 'Kode cabang', 'trim|required');
        $this->form_validation->set_rules('nama_kcp', 'Nama KCP', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('manajemen/office/kcp/c_kcp', $data);
            $this->load->view('layout/footer');
        } else {
            $pic = $this->session->userdata('username');
            $data = [
                'kode_cabang' => htmlspecialchars($this->input->post('kode_cabang', true)),
                'kode_kcp' => htmlspecialchars($this->input->post('kode_kcp', true)),
                'nama_kcp' => htmlspecialchars($this->input->post('nama_kcp', true)),
                'aktif' => 1,
                'date_created' => date('Y-m-d H:i:s')
            ];


            $log = [
                'log' => "Menambahkan Data Cabang ",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('dbm_kcp', $data);
            $this->db->insert('dbm_log', $log);

            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('kcp/view');
        }
    }

    public function update()
    {

        $id_kcp = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Cabang',
            'active_menu_man' => 'open',
            'active_menu_kantor' => 'open',
            'active_menu_kcp' => 'active',
            'kc' => $this->mm->getKc(),
            'd' => $this->mm->getDetailKcp($id_kcp)
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/office/kcp/u_kcp', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {

        $pic = $this->session->userdata('username');
        $id_kcp = htmlspecialchars($this->input->post('id_kcp', true));
        $data = [
            'kode_cabang' => htmlspecialchars($this->input->post('kode_cabang', true)),
            'kode_kcp' => htmlspecialchars($this->input->post('kode_kcp', true)),
            'nama_kcp' => htmlspecialchars($this->input->post('nama_kcp', true)),
        ];

        $log = [
            'log' => "Merubah data Cabang $id_kcp",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateKcp($data,  $id_kcp);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kcp/view');
    }


    public function aktif()
    {
        $id_kcp = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'aktif' => 1,
        ];

        $log = [
            'log' => "Merubah data Cabang $id_kcp",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateKcp($data,  $id_kcp);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kcp/view');
    }


    public function nonaktif()
    {
        $id_kcp = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'aktif' => 0,
        ];

        $log = [
            'log' => "Merubah data Cabang $id_kcp",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateKcp($data,  $id_kcp);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('kcp/view');
    }
}

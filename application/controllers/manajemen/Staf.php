
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Staf extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('Model_Staf', 'mm');
        $this->load->model('Model_Manajemen', 'man');
    }

    public function view()
    {

        $data = array(
            'title' => 'Data User',
            'active_menu_man' => 'open',
            'active_menu_staf' => 'active',
            'v' => $this->mm->getUser()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/staf/v_staf', $data);
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $data = array(
            'title' => 'Data User',
            'active_menu_man' => 'open',
            'active_menu_staf' => 'active',
            'kcp' => $this->man->getKcp()
        );

        $this->form_validation->set_rules('nip', 'NIP', 'required|trim|is_unique[dbm_user.nip]', [
            'is_unique' => 'This NIP has already registered!'
        ]);
        $this->form_validation->set_rules('kode_kcp', 'Kode KCP', 'trim|required');
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required');
        $this->form_validation->set_rules('role_id', 'Role ID', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('manajemen/staf/c_staf', $data);
            $this->load->view('layout/footer');
        } else {
            $pic = $this->session->userdata('username');
            $pass = "banksumut123";
            $role = $this->input->post('kode_kcp', true);
            if ($role == 3) {
                $data = [
                    'kode_kcp' => htmlspecialchars($this->input->post('kode_kcp', true)),
                    'nip' => htmlspecialchars($this->input->post('nip', true)),
                    'username' => htmlspecialchars($this->input->post('kode_kcp', true)),
                    'nama' => htmlspecialchars($this->input->post('nama', true)),
                    'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
                    'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
                    'jk' => htmlspecialchars($this->input->post('jk', true)),
                    'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
                    'role_id' => htmlspecialchars($this->input->post('role_id', true)),
                    'email' => htmlspecialchars($this->input->post('email', true)),
                    'password' => password_hash($pass, PASSWORD_DEFAULT),
                    'is_active' => 1,
                    'image' => "default.jpg",
                    'date_created' => date('Y-m-d H:i:s')
                ];


                $log = [
                    'log' => "Menambahkan Data User",
                    'username' => $pic,
                    'date_created' => date('Y-m-d H:i:s')
                ];
                $this->db->insert('dbm_user', $data);
                $this->db->insert('dbm_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('staf/view');
            } else {
                $data = [
                    'kode_kcp' => htmlspecialchars($this->input->post('kode_kcp', true)),
                    'nip' => htmlspecialchars($this->input->post('nip', true)),
                    'username' => htmlspecialchars($this->input->post('nip', true)),
                    'nama' => htmlspecialchars($this->input->post('nama', true)),
                    'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
                    'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
                    'jk' => htmlspecialchars($this->input->post('jk', true)),
                    'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
                    'role_id' => htmlspecialchars($this->input->post('role_id', true)),
                    'email' => htmlspecialchars($this->input->post('email', true)),
                    'password' => password_hash($pass, PASSWORD_DEFAULT),
                    'is_active' => 1,
                    'image' => "default.jpg",
                    'date_created' => date('Y-m-d H:i:s')
                ];

                $log = [
                    'log' => "Menambahkan Data User",
                    'username' => $pic,
                    'date_created' => date('Y-m-d H:i:s')
                ];
                $this->db->insert('dbm_user', $data);
                $this->db->insert('dbm_log', $log);

                $this->session->set_flashdata('sukses', 'Disimpan');
                redirect('staf/view');
            }
        }
    }

    public function update()
    {

        $id_user = $this->uri->segment(3);
        $data = array(
            'title' => 'Data User',
            'active_menu_man' => 'open',
            'active_menu_staf' => 'active',
            'kcp' => $this->man->getKcp(),
            'd' => $this->mm->detailUser($id_user),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/staf/u_staf', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {

        $pic = $this->session->userdata('username');
        $id_user = htmlspecialchars($this->input->post('id_user', true));
        $data = [
            'kode_kcp' => htmlspecialchars($this->input->post('kode_kcp', true)),
            'nip' => htmlspecialchars($this->input->post('nip', true)),
            'username' => htmlspecialchars($this->input->post('nip', true)),
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
            'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
            'jk' => htmlspecialchars($this->input->post('jk', true)),
            'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'role_id' => htmlspecialchars($this->input->post('role_id', true))
        ];

        $log = [
            'log' => "Merubah data User $id_user",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateUser($data,  $id_user);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('staf/view');
    }


    public function aktif()
    {
        $id_user = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'is_active' => 1,
        ];

        $log = [
            'log' => "Mengaktifkan data User $id_user",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateUser($data,  $id_user);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('staf/view');
    }


    public function nonaktif()
    {
        $id_user = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'is_active' => 0,
        ];

        $log = [
            'log' => "Menonaktfkan data User $id_user",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateUser($data,  $id_user);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('staf/view');
    }

    public function detail()
    {

        $id_user = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail User',
            'active_menu_man' => 'open',
            'active_menu_staf' => 'active',
            'kcp' => $this->man->getKcp(),
            'd' => $this->mm->detailUser($id_user),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('manajemen/staf/d_staf', $data);
        $this->load->view('layout/footer');
    }


    public function resetPassword()
    {
        $id_user = $this->uri->segment(3);
        $pic = $this->session->userdata('username');
        $data = [
            'is_active' => 1,
        ];

        $log = [
            'log' => "Mengaktifkan data User $id_user",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mm->updateUser($data,  $id_user);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('staf/view');
    }
}

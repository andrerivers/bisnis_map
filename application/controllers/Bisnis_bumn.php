
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bisnis_bumn extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('ModelSales', 'ms');
        $this->load->model('ModelNasabah', 'mn');
        $this->load->model('Model_Bisnis_Bumn', 'mbp');
    }

    public function form()
    {
        $data = array(
            'title' => 'Form Bisnis BUMN',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'nasabah' => $this->mbp->getNasabah()
        );
        $this->form_validation->set_rules('nama_instansi', 'Nama Instansi', 'trim|required');
        $this->form_validation->set_rules('alamat_instansi', 'Alamat Instansi', 'trim|required');
        $this->form_validation->set_rules('id_provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_kota', 'Kota', 'trim|required');
        $this->form_validation->set_rules('id_kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('id_kelurahan', 'kelurahan', 'trim|required');
        $this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|required');
        $this->form_validation->set_rules('no_hp_instansi', 'No Hp', 'trim|required');
        $this->form_validation->set_rules('email_instansi', 'Email Instansi', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('company/bisnis_bumn/create', $data);
            $this->load->view('layout/footer');
        } else {
            $pic = $this->session->userdata('username');
            $id = $this->uuid->v4();
            $reff = str_replace('-', '', $id);
            $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));

            $upload_image = $_FILES['file']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('file')) {
                    $old_image = $data['dbm_bisnis_map']['file'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/' . $old_image);
                    }
                    $upload1 = $this->upload->data('file_name');
                    $this->db->set('file', $upload1);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $upload_image = $_FILES['foto']['name'];
            if ($upload_image) {
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size']      = '10000';
                $config['upload_path'] = './archive/';
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('foto')) {
                    $old_image = $data['dbm_bisnis_map']['foto'];
                    if ($old_image != 'default.pdf') {
                        unlink(FCPATH . 'archive/' . $old_image);
                    }
                    $uploadfoto = $this->upload->data('file_name');
                    $this->db->set('foto', $uploadfoto);
                } else {
                    echo $this->upload->display_errors();
                }
            }
            $data = [
                'uid_form' => $reff,
                'nama_instansi' => htmlspecialchars($this->input->post('nama_instansi', true)),
                'alamat_instansi' => htmlspecialchars($this->input->post('alamat_instansi', true)),
                'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
                'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
                'id_kecamatan' => htmlspecialchars($this->input->post('id_kecamatan', true)),
                'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
                'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
                'id_kelurahan' => htmlspecialchars($this->input->post('id_kelurahan', true)),
                'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
                'no_hp_instansi' => htmlspecialchars($this->input->post('no_hp_instansi', true)),
                'email_instansi' => htmlspecialchars($this->input->post('email_instansi', true)),
                'jumlah_instansi' => htmlspecialchars($this->input->post('jumlah_instansi', true)),
                'pgl_bank_sumut' => htmlspecialchars($this->input->post('pgl_bank_sumut', true)),
                'pgl_bank_lain' => htmlspecialchars($this->input->post('pgl_bank_lain', true)),
                'sdm_payroll_noa' => htmlspecialchars($this->input->post('sdm_payroll_noa', true)),
                'sdm_payroll_nominal' => htmlspecialchars($this->input->post('sdm_payroll_nominal', true)),
                'sdm_jumlah_pegawai' => htmlspecialchars($this->input->post('sdm_jumlah_pegawai', true)),
                'sdm_gaji_total_pegawai' => htmlspecialchars($this->input->post('sdm_gaji_total_pegawai', true)),
                'pdjb_giro_noa' => htmlspecialchars($this->input->post('pdjb_giro_noa', true)),
                'pdjb_giro_nominal' => htmlspecialchars($this->input->post('pdjb_giro_nominal', true)),
                'pdjb_tabungan_noa' => htmlspecialchars($this->input->post('pdjb_tabungan_noa', true)),
                'pdjb_tabungan_nominal' => htmlspecialchars($this->input->post('pdjb_tabungan_nominal', true)),
                'pdjb_deposito_noa' => htmlspecialchars($this->input->post('pdjb_deposito_noa', true)),
                'pdjb_deposito_nominal' => htmlspecialchars($this->input->post('pdjb_deposito_nominal', true)),
                'e_cms' => htmlspecialchars($this->input->post('e_cms', true)),
                'e_edc' => htmlspecialchars($this->input->post('e_edc', true)),
                'e_merchent_qris' => htmlspecialchars($this->input->post('e_merchent_qris', true)),
                'e_sumut_mobile' => htmlspecialchars($this->input->post('e_sumut_mobile', true)),
                'e_kartu_atm' => htmlspecialchars($this->input->post('e_kartu_atm', true)),
                'potensi_jumlah_pegawai' => htmlspecialchars($this->input->post('potensi_jumlah_pegawai', true)),
                'potensi_nominal' => htmlspecialchars($this->input->post('potensi_nominal', true)),
                'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
                'catatan' => htmlspecialchars($this->input->post('catatan', true)),
                'link_maps' => htmlspecialchars($this->input->post('link_maps', true)),
                'date_created' => date('Y-m-d H:i:s'),
                'username' => $pic,
                'sts' => 0,
                'file' => $upload1,
                'foto' => $uploadfoto,
                'id_kategori_bisnis' => 24,
                'date_created' => date('Y-m-d H:i:s')
            ];


            $history = [
                'uid_form' => $reff,
                'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
                'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];


            $log = [
                'log' => "Membuat Menambahkan Nasabah dengan $nama_instansi",
                'username' => $pic,
                'date_created' => date('Y-m-d H:i:s')
            ];
            $this->db->insert('dbm_bisnis_map', $data);
            $this->db->insert('dbm_history_map', $history);
            $this->db->insert('dbm_log', $log);

            $this->session->set_flashdata('sukses', 'Disimpan');
            redirect('bisnis_bumn/detail/' . $reff);
        }
    }

    public function detail()
    {
        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Detail Bisnis bumn',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'd' => $this->mbp->detailCompany($uid_form)

        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/bisnis_bumn/detail', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {

        $uid_form = $this->uri->segment(3);
        $data = array(
            'title' => 'Form Bisnis bumn ',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'active_menu_com_draft' => 'active',
            'd' => $this->mbp->detail($uid_form)
        );
        $this->load->view('layout/header', $data);
        $this->load->view('company/bisnis_bumn/update', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {

        $data = array(
            'title' => 'Form Bisnis bumn',
            'active_menu_maping' => 'open',
            'active_menu_form' => 'active',
            'nasabah' => $this->mbp->getNasabah()

        );
        $uid_form = htmlspecialchars($this->input->post('uid_form', true));
        $pic = $this->session->userdata('username');
        $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));

        $data2['detail'] = $this->db->get_where('dbm_bisnis_map',  ['uid_form' => $uid_form])->row_array();

        $uid_form = htmlspecialchars($this->input->post('uid_form', true));
        $pic = $this->session->userdata('username');
        $nama_instansi = htmlspecialchars($this->input->post('nama_instansi', true));

        $upload_image = $_FILES['file']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $old_image = $data2['dbm_bisnis_map']['file'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/' . $old_image);
                }
                $upload1 = $this->upload->data('file_name');
                $this->db->set('file', $upload1);
            } else {
                echo $this->upload->display_errors();
            }
        }

        $upload_image = $_FILES['foto']['name'];
        if ($upload_image) {
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size']      = '10000';
            $config['upload_path'] = './archive/';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('foto')) {
                $old_image = $data2['dbm_bisnis_map']['foto'];
                if ($old_image != 'default.pdf') {
                    unlink(FCPATH . 'archive/' . $old_image);
                }
                $uploadfoto = $this->upload->data('file_name');
                $this->db->set('foto', $uploadfoto);
            } else {
                echo $this->upload->display_errors();
            }
        }
        $data = [
            'nama_instansi' => htmlspecialchars($this->input->post('nama_instansi', true)),
            'alamat_instansi' => htmlspecialchars($this->input->post('alamat_instansi', true)),
            'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
            'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
            'id_kecamatan' => htmlspecialchars($this->input->post('id_kecamatan', true)),
            'id_provinsi' => htmlspecialchars($this->input->post('id_provinsi', true)),
            'id_kota' => htmlspecialchars($this->input->post('id_kota', true)),
            'id_kelurahan' => htmlspecialchars($this->input->post('id_kelurahan', true)),
            'kode_pos' => htmlspecialchars($this->input->post('kode_pos', true)),
            'no_hp_instansi' => htmlspecialchars($this->input->post('no_hp_instansi', true)),
            'email_instansi' => htmlspecialchars($this->input->post('email_instansi', true)),
            'jumlah_instansi' => htmlspecialchars($this->input->post('jumlah_instansi', true)),
            'pgl_bank_sumut' => htmlspecialchars($this->input->post('pgl_bank_sumut', true)),
            'pgl_bank_lain' => htmlspecialchars($this->input->post('pgl_bank_lain', true)),
            'sdm_payroll_noa' => htmlspecialchars($this->input->post('sdm_payroll_noa', true)),
            'sdm_payroll_nominal' => htmlspecialchars($this->input->post('sdm_payroll_nominal', true)),
            'sdm_jumlah_pegawai' => htmlspecialchars($this->input->post('sdm_jumlah_pegawai', true)),
            'sdm_gaji_total_pegawai' => htmlspecialchars($this->input->post('sdm_gaji_total_pegawai', true)),
            'pdjb_giro_noa' => htmlspecialchars($this->input->post('pdjb_giro_noa', true)),
            'pdjb_giro_nominal' => htmlspecialchars($this->input->post('pdjb_giro_nominal', true)),
            'pdjb_tabungan_noa' => htmlspecialchars($this->input->post('pdjb_tabungan_noa', true)),
            'pdjb_tabungan_nominal' => htmlspecialchars($this->input->post('pdjb_tabungan_nominal', true)),
            'pdjb_deposito_noa' => htmlspecialchars($this->input->post('pdjb_deposito_noa', true)),
            'pdjb_deposito_nominal' => htmlspecialchars($this->input->post('pdjb_deposito_nominal', true)),
            'e_cms' => htmlspecialchars($this->input->post('e_cms', true)),
            'e_edc' => htmlspecialchars($this->input->post('e_edc', true)),
            'e_merchent_qris' => htmlspecialchars($this->input->post('e_merchent_qris', true)),
            'e_sumut_mobile' => htmlspecialchars($this->input->post('e_sumut_mobile', true)),
            'e_kartu_atm' => htmlspecialchars($this->input->post('e_kartu_atm', true)),
            'potensi_jumlah_pegawai' => htmlspecialchars($this->input->post('potensi_jumlah_pegawai', true)),
            'potensi_nominal' => htmlspecialchars($this->input->post('potensi_nominal', true)),
            'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
            'catatan' => htmlspecialchars($this->input->post('catatan', true)),
            'link_maps' => htmlspecialchars($this->input->post('link_maps', true)),
            'date_created' => date('Y-m-d H:i:s'),
            'username' => $pic,
            'sts' => 0,
            'file' => $upload1,
            'foto' => $uploadfoto,
            'id_kategori_bisnis' => 24
        ];


        $history = [
            'uid_form' => $uid_form,
            'sts_pipeline' => htmlspecialchars($this->input->post('sts_pipeline', true)),
            'keterangan' => htmlspecialchars($this->input->post('keterangan', true)),
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];


        $log = [
            'log' => "Membuat Menambahkan Nasabah dengan $nama_instansi",
            'username' => $pic,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mbp->update($data,  $uid_form);
        $this->db->insert('dbm_history_map', $history);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('bisnis_bumn/detail/' . $uid_form);
    }
}

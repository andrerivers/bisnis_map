<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        $this->load->library('uuid');
        $this->load->model('Model_Manajemen', 'man');
        $this->load->model('ModelProfil', 'mp');
    }



    public function index()
    {
        $data = array(
            'title' => 'Dashboard',
            'active_menu_dashboard' => 'active',
        );
        $this->load->view('layout/header', $data);
        $this->load->view('sales/index', $data);
        $this->load->view('layout/footer');
    }


    public function profil()
    {
        $data = array(
            'title' => 'Profil',
            'active_menu_pengaturan' => 'open',
            'active_menu_uprofil' => 'active',
            'd' => $this->mp->detailProfil(),
        );
        $this->load->view('layout/header', $data);
        $this->load->view('profil/d_profil', $data);
        $this->load->view('layout/footer');
    }

    public function update()
    {
        $data = array(
            'title' => 'Update Profil',
            'active_menu_pengaturan' => 'open',
            'active_menu_uprofil' => 'active',
            'd' => $this->mp->detailProfil()
        );
        $this->load->view('layout/header', $data);
        $this->load->view('profil/u_profil', $data);
        $this->load->view('layout/footer');
    }

    public function updateGo()
    {

        $username = $this->session->userdata('username');
        $id_user = htmlspecialchars($this->input->post('id_user', true));
        $data = [
            'nama' => htmlspecialchars($this->input->post('nama', true)),
            'tempat_lahir' => htmlspecialchars($this->input->post('tempat_lahir', true)),
            'tgl_lahir' => htmlspecialchars($this->input->post('tgl_lahir', true)),
            'jk' => htmlspecialchars($this->input->post('jk', true)),
            'no_hp' => htmlspecialchars($this->input->post('no_hp', true)),
            'email' => htmlspecialchars($this->input->post('email', true))
        ];

        $log = [
            'log' => "Merubah data User $id_user",
            'username' => $username,
            'date_created' => date('Y-m-d H:i:s')
        ];

        $result = $this->mp->updateProfil($data,  $id_user);
        $this->db->insert('dbm_log', $log);

        $this->session->set_flashdata('sukses', 'Disimpan');
        redirect('sales/profil');
    }



    public function form()
    {
        $data = array(
            'title' => 'Form',
            'active_menu_rpt' => 'active',

        );
        $this->load->view('layout/header', $data);
        $this->load->view('sales/form', $data);
        $this->load->view('layout/footer');
    }

    public function f_contact()
    {
        $data = array(
            'title' => 'Form',
            'active_menu_rpt' => 'active',
            'bisnis' => $this->ms->getBisnis(),
            'dpk' => $this->ms->getDpk()

        );
        $this->load->view('layout/header', $data);
        $this->load->view('sales/contact/f_contact', $data);
        $this->load->view('layout/footer');
    }


    public function password()
    {
        $data = array(
            'title' => 'Ganti Password',
            'active_menu_pengaturan' => 'open',
            'active_menu_upass' => 'active',
            'd' => $this->mp->detailProfil(),
        );

        $data2['user'] = $this->db->get_where('dbm_user',  ['username' => $this->session->userdata('username')])->row_array();

        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[6]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[6]|matches[new_password1]');

        if ($this->form_validation->run() == false) {
            $this->load->view('layout/header', $data);
            $this->load->view('profil/c_password', $data);
            $this->load->view('layout/footer');
        } else {
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password1');
            if (!password_verify($current_password, $data2['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
           Kata Sandi Salah Saat ini ! </div>');
                redirect('sales/password');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Kata sandi baru tidak boleh sama dengan kata sandi saat ini! </div>');
                    redirect('sales/password');
                } else {
                    // password sudah ok
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                    $this->db->set('password', $password_hash);
                    $this->db->where('username', $this->session->userdata('username'));
                    $this->db->update('dbm_user');

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Ganti Kata sandi baru Berhasil! </div>');
                    redirect('sales/password');
                }
            }
        }
    }
}

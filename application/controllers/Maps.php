<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Maps extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("logged") <> 1) {
            redirect(site_url('auth'));
        }
        //Do your magic here
        $this->load->library(array("googlemaps"));
        $this->load->model('ModelSetting', 'mset');
        $this->load->model('ModelSales', 'ms');
        $this->load->model('ModelNasabah', 'mn');
    }


    public function index()
    {
        $setting = $this->mset->list_setting();
        //tampilan awal view map
        $this->load->library("googlemaps");
        $config['center'] = "$setting->latitude,$setting->longitude";
        $config['zoom'] = "$setting->zoom";
        $this->googlemaps->initialize($config);
        //tampil semua data sekolah dari database 
        $nasabah = $this->mn->getNasabah();
        foreach ($nasabah as $key => $value) : //perulangan data
            $marker = array();
            $marker['animation'] = 'DROP';
            $marker['position'] = $value['latitude'] . ',' . $value['longitude'];
            $marker['infowindow_content'] = '<div class="media" style="width:400px;">';
            $marker['infowindow_content'] .= '<div class="media-left">';
            $marker['infowindow_content'] .= '<img src="' . base_url("archive/{$value['photo']}") . '" class="media-object" style="width:150px">';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '<div class="media-body">';
            $marker['infowindow_content'] .= '<h4 class="media-heading">' . $value['nama'] . '</h4>';
            $marker['infowindow_content'] .= '<p>Alamat : ' . $value['alamat'] . '</p>';
            $marker['infowindow_content'] .= '<p>No Telpon : ' . $value['no_hp'] . '</p>';
            $marker['infowindow_content'] .= '<p>Email' . $value['email'] . '</p><br>';
            $marker['infowindow_content'] .= '<a href="' . base_url('sales/nasabah/detail/' . $value['uid_nasabah']) . '" class="btn btn-success btn-sm"><i class="fa fa-list"></i> Detail</a>';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '</div>';
            $marker['icon'] = base_url('assets/icon/') . $value['icon'];
            $this->googlemaps->add_marker($marker);
        endforeach;
        //end perulangan data
        //tampilan data marker map
        $this->googlemaps->initialize($config);
        $map = $this->googlemaps->create_map();
        //menampilkan maker ke map

        $data = array(
            'title' => 'Maping Nasabah Perorangan',
            'active_menu_mapsPerorangan' => 'active',
            'bisnis' => $this->mn->getBisnis(),
            'jt' => $this->mn->getJenisTab(),
            'map' => $map,

        );

        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/v_maping', $data);
        $this->load->view('layout/footer');
    }

    public function maps_bisnis()
    {
        $setting = $this->mset->list_setting();
        //tampilan awal view map
        $this->load->library("googlemaps");
        $config['center'] = "$setting->latitude,$setting->longitude";
        $config['zoom'] = "$setting->zoom";
        $this->googlemaps->initialize($config);
        //tampil semua data sekolah dari database 
        $nasabah = $this->mn->getMapsBisnis();
        foreach ($nasabah as $key => $value) : //perulangan data
            $marker = array();
            $marker['animation'] = 'DROP';
            $marker['position'] = $value['latitude'] . ',' . $value['longitude'];
            $marker['infowindow_content'] = '<div class="media" style="width:400px;">';
            $marker['infowindow_content'] .= '<div class="media-left">';
            $marker['infowindow_content'] .= '<img src="' . base_url("archive/{$value['foto']}") . '" class="media-object" style="width:150px">';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '<div class="media-body">';
            $marker['infowindow_content'] .= '<h4 class="media-heading">' . $value['nama_instansi'] . '</h4>';
            $marker['infowindow_content'] .= '<p>Alamat : ' . $value['alamat_instansi'] . '</p>';
            $marker['infowindow_content'] .= '<p>No Telpon : ' . $value['no_hp_instansi'] . '</p>';
            $marker['infowindow_content'] .= '<p>Email' . $value['email_instansi'] . '</p><br>';
            $marker['infowindow_content'] .= '<a href="' . base_url('sales/nasabah/detail/' . $value['uid_form']) . '" class="btn btn-success btn-sm"><i class="fa fa-list"></i> Detail</a>';
            $marker['infowindow_content'] .= '</div>';
            $marker['infowindow_content'] .= '</div>';
            $marker['icon'] = base_url('assets/icon/') . $value['icon'];
            $this->googlemaps->add_marker($marker);
        endforeach;
        //end perulangan data
        //tampilan data marker map
        $this->googlemaps->initialize($config);
        $map = $this->googlemaps->create_map();
        //menampilkan maker ke map

        $data = array(
            'title' => 'Maping Nasabah Bisnis',
            'active_menu_mapsBisnis' => 'active',
            'bisnis' => $this->mn->getBisnis(),
            'jt' => $this->mn->getJenisTab(),
            'map' => $map,

        );

        $this->load->view('layout/header', $data);
        $this->load->view('nasabah/v_maping', $data);
        $this->load->view('layout/footer');
    }
}

/* End of file Admin.php */

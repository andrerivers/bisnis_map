<?php

defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'Auth/index';
$route['translate_uri_dashes'] = FALSE;
//===================================================
//------------------AUTH----------------------------
//===================================================
$route['auth'] = 'Auth/index';
$route['forgotPassword'] = 'Auth/forgotPassword';
$route['auth/logout'] = 'Auth/logout';

$route['sales/profil'] = 'Sales/profil';
$route['sales/update'] = 'Sales/update';
$route['sales/password'] = 'Sales/password';

//===================================================
//------------------SALES----------------------------
//===================================================

$route['sales'] = 'Sales/index';
$route['sales/form'] = 'Sales/form';
$route['sales/f_contact'] = 'Sales/f_contact';
$route['sales/f_qualification'] = 'Sales/f_qualification';
$route['sales/f_meeting'] = 'Sales/f_meeting';
$route['sales/f_proposal'] = 'Sales/f_proposal';
$route['sales/f_closing'] = 'Sales/f_closing';
$route['sales/f_retention'] = 'Sales/f_retention';


$route['sales/nasabah'] = 'Nasabah/v_nasabah';
$route['sales/nasabah/create'] = 'Nasabah/c_nasabah';
$route['sales/nasabah/updated'] = 'Nasabah/u_nasabah';
$route['sales/nasabah/detail/(:any)'] = 'Nasabah/d_nasabah/(:any)';
$route['sales/nasabah/update/(:any)'] = 'Nasabah/u_nasabah/(:any)';
$route['sales/nasabah/maping/(:any)'] = 'Nasabah/maping/(:any)';
$route['sales/nasabah/updateGo'] = 'Nasabah/updateGo';
$route['sales/nasabah/updateMap'] = 'Nasabah/updateMap';
$route['sales/maps'] = 'Maps/index';
$route['sales/maps_bisnis'] = 'Maps/maps_bisnis';
$route['sales/buat_maps/(:any)'] = 'Nasabah/maping_bisnis/(:any)';
$route['sales/nasabah/maping_bisnisGo'] = 'Nasabah/maping_bisnisGo';

$route['prospecting/view'] = 'Prospecting/view';
$route['prospecting/create'] = 'Prospecting/create';
$route['prospecting/detail/(:any)'] = 'Prospecting/detail/(:any)';
$route['prospecting/update/(:any)'] = 'Prospecting/update/(:any)';
$route['prospecting/updateGo'] = 'Prospecting/updateGo';
$route['prospecting/switch'] = 'Prospecting/switch';


$route['qualification/view'] = 'Qualification/view';
$route['qualification/create'] = 'Qualification/create';
$route['qualification/detail/(:any)'] = 'Qualification/detail/(:any)';
$route['qualification/update/(:any)'] = 'Qualification/update/(:any)';
$route['qualification/updateGo'] = 'Qualification/updateGo';
$route['qualification/switch'] = 'Qualification/switch';


$route['meeting/view'] = 'Meeting/view';
$route['meeting/create'] = 'Meeting/create';
$route['meeting/detail/(:any)'] = 'Meeting/detail/(:any)';
$route['meeting/update/(:any)'] = 'Meeting/update/(:any)';
$route['meeting/updateGo'] = 'Meeting/updateGo';
$route['meeting/switch'] = 'Meeting/switch';

$route['proposal/view'] = 'Proposal/view';
$route['proposal/create'] = 'Proposal/create';
$route['proposal/detail/(:any)'] = 'Proposal/detail/(:any)';
$route['proposal/update/(:any)'] = 'Proposal/update/(:any)';
$route['proposal/updateGo'] = 'Proposal/updateGo';
$route['proposal/switch'] = 'Proposal/switch';

$route['closing/view'] = 'Closing/view';
$route['closing/create'] = 'Closing/create';
$route['closing/detail/(:any)'] = 'Closing/detail/(:any)';
$route['closing/update/(:any)'] = 'Closing/update/(:any)';
$route['closing/updateGo'] = 'Closing/updateGo';
$route['closing/switch'] = 'Closing/switch';

$route['retention/view'] = 'Retention/view';
$route['retention/create'] = 'Retention/create';
$route['retention/detail/(:any)'] = 'Retention/detail/(:any)';
$route['retention/update/(:any)'] = 'Retention/update/(:any)';
$route['retention/updateGo'] = 'Retention/updateGo';
$route['retention/switch'] = 'Retention/switch';


$route['company/form'] = 'Company/form';
$route['company/draft'] = 'Company/view_draft';
$route['company/hot'] = 'Company/view_hot';
$route['company/cold'] = 'Company/view_cold';

// BISNIS PEMERINTAH
$route['bisnis_pemerintah/create'] = 'Bisnis_pemerintah/form';
$route['bisnis_pemerintah/detail/(:any)'] = 'Bisnis_pemerintah/detail/(:any)';
$route['bisnis_pemerintah/update/(:any)'] = 'Bisnis_pemerintah/update/(:any)';
$route['bisnis_pemerintah/updateGo'] = 'Bisnis_pemerintah/updateGo';

// BISNIS PENDIDIKAN
$route['bisnis_pendidikan/create'] = 'Bisnis_pendidikan/form';
$route['bisnis_pendidikan/detail/(:any)'] = 'Bisnis_pendidikan/detail/(:any)';
$route['bisnis_pendidikan/update/(:any)'] = 'Bisnis_pendidikan/update/(:any)';
$route['bisnis_pendidikan/updateGo'] = 'Bisnis_pendidikan/updateGo';

// BISNIS PASAR
$route['bisnis_pasar/create'] = 'Bisnis_pasar/form';
$route['bisnis_pasar/detail/(:any)'] = 'Bisnis_pasar/detail/(:any)';
$route['bisnis_pasar/update/(:any)'] = 'Bisnis_pasar/update/(:any)';
$route['bisnis_pasar/updateGo'] = 'Bisnis_pasar/updateGo';


// BISNIS KESEHATAN
$route['bisnis_kesehatan/create'] = 'Bisnis_kesehatan/form';
$route['bisnis_kesehatan/detail/(:any)'] = 'Bisnis_kesehatan/detail/(:any)';
$route['bisnis_kesehatan/update/(:any)'] = 'Bisnis_kesehatan/update/(:any)';
$route['bisnis_kesehatan/updateGo'] = 'Bisnis_kesehatan/updateGo';

// BISNIS BUMN
$route['bisnis_bumn/create'] = 'Bisnis_bumn/form';
$route['bisnis_bumn/detail/(:any)'] = 'Bisnis_bumn/detail/(:any)';
$route['bisnis_bumn/update/(:any)'] = 'Bisnis_bumn/update/(:any)';
$route['bisnis_bumn/updateGo'] = 'Bisnis_bumn/updateGo';


// BISNIS PERKEBUNAN/PETERNAKAN/PERIKANAN
$route['bisnis_perkebunan/create'] = 'Bisnis_perkebunan/form';
$route['bisnis_perkebunan/detail/(:any)'] = 'Bisnis_perkebunan/detail/(:any)';
$route['bisnis_perkebunan/update/(:any)'] = 'Bisnis_perkebunan/update/(:any)';
$route['bisnis_perkebunan/updateGo'] = 'Bisnis_perkebunan/updateGo';




// MANAJEMEN
$route['kc/create'] = 'manajemen/Kc/create';
$route['kc/view'] = 'manajemen/Kc/view';
$route['kc/update/(:any)'] = 'manajemen/Kc/update/(:any)';
$route['kc/aktif/(:any)'] = 'manajemen/Kc/aktif/(:any)';
$route['kc/nonaktif/(:any)'] = 'manajemen/Kc/nonaktif/(:any)';
$route['kc/updateGo'] = 'manajemen/Kc/updateGo';


$route['kcp/create'] = 'manajemen/Kcp/create';
$route['kcp/view'] = 'manajemen/Kcp/view';
$route['kcp/update/(:any)'] = 'manajemen/Kcp/update/(:any)';
$route['kcp/aktif/(:any)'] = 'manajemen/Kcp/aktif/(:any)';
$route['kcp/nonaktif/(:any)'] = 'manajemen/Kcp/nonaktif/(:any)';
$route['kcp/updateGo'] = 'manajemen/Kcp/updateGo';


$route['kb/view'] = 'manajemen/Kb/view';
$route['kb/create'] = 'manajemen/Kb/create';
$route['kb/update/(:any)'] = 'manajemen/Kb/update/(:any)';
$route['kb/delete/(:any)'] = 'manajemen/Kb/delete/(:any)';
$route['kb/updateGo'] = 'manajemen/Kb/updateGo';

$route['staf/view'] = 'manajemen/Staf/view';
$route['staf/create'] = 'manajemen/Staf/create';
$route['staf/update/(:any)'] = 'manajemen/Staf/update/(:any)';
$route['staf/detail/(:any)'] = 'manajemen/Staf/detail/(:any)';
$route['staf/resetPassword/(:any)'] = 'manajemen/Staf/resetPassword/(:any)';
$route['staf/updateGo'] = 'manajemen/Staf/updateGo';
$route['staf/aktif/(:any)'] = 'manajemen/Staf/aktif/(:any)';
$route['staf/nonaktif/(:any)'] = 'manajemen/Staf/nonaktif/(:any)';

$route['staf_cabang/view'] = 'manajemen/Staf_cabang/view';
$route['staf_cabang/create'] = 'manajemen/Staf_cabang/create';
$route['staf_cabang/update/(:any)'] = 'manajemen/Staf_cabang/update/(:any)';
$route['staf_cabang/detail/(:any)'] = 'manajemen/Staf_cabang/detail/(:any)';
$route['staf_cabang/resetPassword/(:any)'] = 'manajemen/Staf_cabang/resetPassword/(:any)';
$route['staf_cabang/updateGo'] = 'manajemen/Staf_cabang/updateGo';
$route['staf_cabang/aktif/(:any)'] = 'manajemen/Staf_cabang/aktif/(:any)';
$route['staf_cabang/nonaktif/(:any)'] = 'manajemen/Staf_cabang/nonaktif/(:any)';


//PROGRESSS UMUM
$route['company/viewProgress/(:any)'] = 'Company/viewProgress/(:any)';
$route['company/createProgress/(:any)'] = 'Company/createProgress/(:any)';
$route['company/updateProgressGo'] = 'Company/updateProgressGo';
$route['company/updateDraft'] = 'Company/updateDraft';



$route['admin_cabang'] = 'Admin_cabang/index';
$route['admin_pusat'] = 'Admin_pusat/index';

$route['(:any)'] = 'errors/show_404';
$route['(:any)/(:any)'] = 'errors/show_404';
$route['(:any)/(:any)/(:any)'] = 'errors/show_404';
